#include <stdio.h>
#include <stdlib.h>

#include "udp/communication/mainprocess.h"

#define DEFAULT_TABLE_FILE "files/table"
#define DEFAULT_DATA "Bonjour"

int main(int argc, char *argv[]) {

	if(argc > 1 && strlen("-h") == strlen(argv[1]) && memcmp(argv[1], "-h", strlen("-h")) == 0) {
		printf("-a <fichier d'adresses> // pour specifier le fichier contenant les adresses\n");
		printf("-d <donnee> // pour specifier la donnee a partager\n");
		printf("-m <adresse multicast ipv6>\n");
		printf("-mp <port de multicast>\n");
		printf("-mi <nom de l'interface de communication>\n");
		return 0;
	}
	if(argc > 1) {
		// Nous avons un port
		char * port = argv[1];
		
		MainProcess * mainProcess;
		char * multicastAddress = NULL;
		int multicastPort = 0;
		char * multicastInterface = NULL;
		if(argc > 3) {
			char * file = NULL;
			char * data = NULL;
			for (int i = 2; i < argc; i++) {
				if(strlen("-a") == strlen(argv[i]) && memcmp(argv[i], "-a", strlen("-a")) == 0) {
					// On passe le fichier d'adresse
					if(argc > i+1)
						file = argv[i+1];
				}
				else if(strlen("-d") == strlen(argv[i]) && memcmp(argv[i], "-d", strlen("-d")) == 0) {
					// On passe la donnee
					if(argc > i+1)
						data = argv[i+1];
				}
				else if(strlen("-m") == strlen(argv[i]) && memcmp(argv[i], "-m", strlen("-m")) == 0) {
					// On passe l'adresse de multicast
					if(argc > i+1)
						multicastAddress = argv[i+1];
				}
				else if(strlen("-mp") == strlen(argv[i]) && memcmp(argv[i], "-mp", strlen("-mp")) == 0) {
					// On passe le port de multicast
					if(argc > i+1)
						multicastPort = atoi(argv[i+1]);
				}
				else if(strlen("-mi") == strlen(argv[i]) && memcmp(argv[i], "-mi", strlen("-mi")) == 0) {
					// On passe le nom de l'interface de multicast
					if(argc > i+1)
						multicastInterface = argv[i+1];
				}
			}
			
			if(file == NULL && data == NULL) {
				mainProcess = main_process_create(DEFAULT_TABLE_FILE, port, DEFAULT_DATA, multicastAddress, multicastPort, multicastInterface);
			}
			else if(file == NULL && data != NULL) {
				mainProcess = main_process_create(DEFAULT_TABLE_FILE, port, data, multicastAddress, multicastPort, multicastInterface);
			}
			else if(file != NULL && data == NULL) {
				mainProcess = main_process_create(file, port, DEFAULT_DATA, multicastAddress, multicastPort, multicastInterface);
			}
			else {
				mainProcess = main_process_create(file, port, data, multicastAddress, multicastPort, multicastInterface);
			}
		}
		else {
			// On n'a pas de fichier de voisins, on utilise le fichier et la donnee par defaut
			mainProcess = main_process_create(DEFAULT_TABLE_FILE, port, DEFAULT_DATA, multicastAddress, multicastPort, multicastInterface);
		}

		if(mainProcess != NULL) {
			main_process(mainProcess);
			main_destroy(mainProcess);
		}
	}
	else {
		printf("Passez un numero de port en premier parametre\n");
	}
	return 0;
}
