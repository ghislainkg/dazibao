#ifndef _GESTION_TABLE_
#define _GESTION_TABLE_

#include <arpa/inet.h>

#include "requete.h"

#define TRACE_LOCATION "files/donnees.txt"

/*Initialise une table de donnees vide*/
table* init(char* message);

/*Affiche la table des donnees t*/
void print_table(table* t);

/*Ecris tous les noeud de la table table dans le fichier TRACE_LOCATION*/
void garder_trace(table* t);

/*Tronque la message pour la faire avoir 192 caracteres maximum.
Si message est de longueur 0, un message par defaut de taille inferieure a 192 est mis.
message doit pointer vers un espace de taille au moins 192*/
void modelage(char* message);

#endif