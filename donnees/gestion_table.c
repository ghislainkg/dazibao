#include "gestion_table.h"

/*Initialise une table de donnees vide.
Retourne NULL en cas d'echec et un pointeur vers la table 
en cas de reussite.*/
table* init(char* message){
	printf("On initie notre table de données et notre nœud\n");

	/*On alloue de la memoire pour les noeuds de la table*/
	void* start=malloc(TAILLE_MALLOC);
	memset(start, 0, TAILLE_MALLOC);
	if(start==NULL){
		printf("Echec Malloc");
		return NULL;
	}
	memset(start, 0, TAILLE_MALLOC);

	// On place notre propre noeud dans la table de donnees
	table* tab=(table*)start;
	table t={(noeud*)((char*)start+sizeof(table)),NULL,"",1};
	noeud n= {ID, htons(0),"", NULL};

	// On tronque le message
	modelage(message);
	// On met a jour le message de notre noeud
	memmove(n.donnee, message, strlen(message));

	// 
	hash_node(&n);
	t.ajour = 0 ;
	*(noeud*)((char*)start+sizeof(table))=n;
	*tab=t;
	tab->free = (void*)((char*)start+sizeof(table) +sizeof(noeud));
	return tab;
}

/*Petmet d'ajouter le noeud n a la table s'il n'y est pas encore,
et le mettre a jour s'il y est deja. t->ajour est mis a vrai*/
void actualiser(table* t, noeud* n) {
	printf("On actualise la table\n");
	noeud *j= t->first;
	noeud *i= t->first;
	noeud *k;

	// On verifie qu'on a assez d'espace pour le  nouveau noeud.
	if ((signed long)(TAILLE_MALLOC-(((t->nb_noeuds)+1)*sizeof(noeud))-sizeof(table)) < 0) {
		// Si on n'a plus assez d'espace, on double la taille de la memoire
		printf("On vient de doubler la mémoire");
		t->free = malloc(TAILLE_MALLOC);
		if (t->free == NULL){
			printf("Echec extension");
			printf("Il y a trop de noeuds sur ce réseau, il est conseillé d'augmenter données/propre.h : TAILLE MALLOC");
			exit(0);
		}
	}
	k = t->free;
	
	/*On ajoute le noeud en premiere position,
	S'il doit etre en premiere position
	(Le table des donnees est trie par id)*/
	if ((i->id) > (n->id)){
		// On cree un nouveau noeud contenant n et dont le suivant est i
		noeud m = {.id=n->id,.sequence= n->sequence,.donnee="",.suivant=i};
		memmove(m.donnee, n->donnee, 192);
		hash_node(&m);
		*k=m;
		// On l'ajoute comme premier
		t->first = k;
		t->free = (void*)((char*)t->free + sizeof(noeud));
		(t->nb_noeuds)++;
		// On dit qu'on est a jour
		t->ajour = 0;
		return;
	}

	/*Si le noeud n'est pas ajoute en premiere position,
	On l'ajoute ou le met a jour a la position a laquelle il doit etre.*/

	// On vas jusqu'a sa position.
	// i contient le noeud avant ou equivalent a n
	// j le noeud avant i
	while((i->id) <= (n->id) && i->suivant != NULL){
		j=i;
		i=i->suivant;
	}

	// Si i est le noeud avant, on ajoute n apres i, (dont a la fin)
	if ((i->id) < (n->id)) {
		// On cree un nouveau noeud contenant n et dont le suivant est NULL
		noeud m= {.id=n->id,.sequence= n->sequence,.donnee="",NULL};
		memmove(m.donnee, n->donnee, 192);
		hash_node(&m);
		*k=m;
		// On l'ajoute apres i
		i->suivant = k;
		t->free = (void*)((char*)t->free + sizeof(noeud));
		(t->nb_noeuds)++;
		// On dit qu'on est a jour
		t->ajour = 0;
		return;
	}
	// Si i est equivalent a n, on le met a jour si necessaire
	if ((i->id)==(n->id)){
		if(( ((ntohs(n->sequence))-(ntohs(i->sequence)))& 32768 )==0){
			// On change la donne de j
			memmove(i->donnee, n->donnee, 192);
			// On recalcul son hash
			hash_node(i);
			// On dit qu'on est a jour
			t->ajour = 0 ;
		}
		return;
	}
	// Si n doit etre entre i et j, on le place apres j
	if ((j->id)<(n->id)){
		// On cree le nouveau noeud conteneant n et dont le suivant est i
		noeud m= {.id=n->id,.sequence= n->sequence,.donnee="",.suivant=i};
		memmove(m.donnee, n->donnee, 192);
		hash_node(&m);
		*k=m;
		// On l'ajoute apres j
		j->suivant = k;
		t->free = (void*)((char*)t->free + sizeof(noeud));
		(t->nb_noeuds)++;
		// On dit qu'on est a jour
		t->ajour = 0;
		return;
	}
	// Ici j est equivalent a n. s'il n'ont pas le meme numero de sequence,
	// On met a jour j avec n
	if((((ntohs(n->sequence))-(ntohs(j->sequence))) & 32768 )==0){
		memmove(j->donnee, n->donnee, 192);
		hash_node(j);
		t->ajour = 0 ;
	}
	return;
}

/*Affiche la table des donnees t*/
void print_table(table* t){
	noeud* n = t->first;
	int k=0;
	while( n != NULL){
		printf("Noeud n° %d",k);
		print(n);
		n = n->suivant ;
		k++;
	}
}

/*Ecris tous les noeud de la table table dans le fichier TRACE_LOCATION*/
void garder_trace(table* t){
	printf("Faisons une sauvegarde\n");
	FILE * fp;
	noeud* n = t->first;
	fp = fopen (TRACE_LOCATION, "a");
	while ( n != NULL){
		fprintf(fp, "Noeud d'id= %lx \n seq= %d \n donnée= %s \n hash = ",n->id,ntohs(n->sequence), n->donnee);
		for (int i=0;i<16;i++)
		fprintf(fp, "%u|",(n->hash)[i]);
		fprintf(fp, "\n\n\n");
		n = n->suivant;
	}
	fclose(fp);
}

/*Tronque la message pour la faire avoir 192 caracteres maximum.
Si message est de longueur 0, un message par defaut de taille inferieure a 192 est mis.
message doit pointer vers un espace de taille au moins 192*/
void modelage(char* message){
	if (strlen(message)>192)
		message[192]='\0';
	if (strlen(message)==0)
		message="phase de test, le message vide se change en celui la";
}
