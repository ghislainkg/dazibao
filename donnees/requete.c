#include "requete.h"

/*Retourne l'indice de la requete reponse courante*/
static int get_indice(traitement* trait, int k){
    int longueur= TAILLE_DATAGRAMME - k;
    for (int i=0; i<NB_REQUETE_REPONSE; i++){
      	if ( ((trait->reponse)[i]).length <= longueur){
        	if(trait->nbrep < i+1) {
				trait->nbrep = i+1;
			}
			return i;
      	}
    }
    return -1;
}

/*Verifi si a est supperieur a b*/
static int sup_or_equal(uint16_t a, uint16_t b){
    return (((ntohs(a)-ntohs(b)) & 32768) == 0 );
}

/*Verifi si a est supperieur strictement a b*/
static int sup_strict(uint16_t a, uint16_t b){
    return (((ntohs(a)-ntohs(b)-1) & 32768) == 0 );
}

/*Verifi si a est egal a b*/
static int compare(unsigned char* a, unsigned char* b){
	for (int i=0; i<16; i++){
		if ( a[i] != b[i] )
			return 0;
	}
    return 1;
}

/*Retourne une Neighbour request*/
requete * neighbour_request() {
    requete * r = malloc(sizeof(requete));
    r->length = 2;
    r->body[0] = 2;
    r->body[1] = 0;
    return r;
}

/*Retourne une Network State Request*/
requete * network_state_request() {
    requete * r = malloc(sizeof(requete));
    r->length = 2;
    r->body[0] = 5;
    r->body[1] = 0;
    return r;
}

/*Retourne une Network Hash contenant le hash de notre table,
apres l'avoir recalculer si nptre table n'est pas a jour*/
requete* network_hash_send(table* t){
    requete * r = malloc(sizeof(requete));
    r->length = 18;
    r->body[0] = 4;
    r->body[1] = 16;
    if(t->ajour != 1) {
		// On n'est pas a jour, on recalcul
      	network_hash(t);
	}
    memmove( &((r->body)[2]), t->hash, 16);
    return r;
}

/*Calcul et met a jour le hash du reseau dans la table t*/
void network_hash(table* t){
	t->ajour = 1;
	printf("On calcul le hash du réseau\n");
	// Vas contenir les hash des tous les noeuds a la suite
	unsigned char temp[(t->nb_noeuds)*16];
	unsigned char *hashNoeud;
	// On met les hash des noeud de la table les un a la suite des autres dans temp 
	noeud *n = t->first;
	int k = 0;
	while (n != NULL){
		hashNoeud = n->hash;
		memmove( &temp[(16*k)], hashNoeud,16);
		k++;
		n = n->suivant;
	}
	// Le nouveau hash vas etre le hash de temp
	h((char*)temp, (t->nb_noeuds)*16, (char*)t->hash); 
}

/*Affiche un noeud*/
void print(noeud* n){
  	printf("Noeud d'id= %lx \n seq= %d \n donnée= %s \n suivant=%p \n avec addr=%p|\n\n",n->id,ntohs(n->sequence), n->donnee, n->suivant, n);
}

/*Retourne le noeud de la table t dont l'id est passe en parametre.
Retourne NULL s'il n'existe pas*/
noeud* get_node(table* t, uint64_t id){
    noeud* i = (t->first);
    while ( i != NULL ){
		if (i->id == id)
			return i;
		i = i->suivant; 
    }
    return NULL;
}

/*Fait un hash de str (qui est de taille len), et l'ecris dans adr.
retourne 1 si tout se passe bien
et -1 en cas d'erreur.*/
int h(const char* str, int len, char* adr ){
	printf("SHA256\n");
	SHA256Context ctx;
	unsigned char hash[32];
	/*Initialisation du context*/
	int rc = SHA256Reset(&ctx);
	if(rc < 0){
		printf("fail 1");
		return -1;
	}
	/*Ajout de str dans le context pour le hasher*/
	rc = SHA256Input(&ctx,(unsigned char *) str, len);
	if(rc < 0){
		printf("fail 2");
		return -1;
	}
	/*On hashe et on obtient le resultat*/
	rc = SHA256Result(&ctx, hash);
	if(rc != 0){
		printf("fail 3");
		return -1;
	}
	/*On ecris le resultat dans adr*/
	memmove(adr, hash, 16);
	return 1;
}

/*Calcul le hash de la donnee du noeud et le met a jour*/
void hash_node(noeud* n){
	printf("On calcul le hash d'un noeud\n");
	int res = h((char*)&(n->id), 10+strlen(n->donnee),(char*) n->hash);
	if(res < 0) {
		printf("Erreur de calcul de hash");
	}
}


/*Traitement d'une Neighbour request.
Ajoute a la reponse du traitement 
l'adresse ip et le port d'un voisin aleatoire de la table des voisins tv*/
void receive_neighbour_request(traitement* trait, TableVoisins* tv){
    printf("On recoit une demande de voisin\n");
    /*On ajoute une voisin aleatoire a la reponse du traitement*/
    send_neighbour(trait, tv);
}

/*Traitement d'une Neighbour.
info doit contenir une adresse ip et un port a la suite.
Un voisin est cree et ajoute a la table des voisin a partir de ces ip et port*/
void receive_neighbour(traitement* trait, char* info, TableVoisins* tv){
    printf("On recoit un voisin\n");
    // On recupere le nom du voisin
    char * name = malloc(100);
    inet_ntop(AF_INET6, info, name, 100);
    // On recupere son port
    uint16_t * port = (uint16_t*) (info+16);

    // On recupere sa sockaddr et on en profite pour le valider
    struct sockaddr_in6 * address = malloc(sizeof(struct sockaddr_in6));
    Error error;
    printf("port = %d\n", ntohs(*port));
    udp_get_sockaddr_portnumber(name, ntohs(*port), address, &error);
    if(error != NO) {
		printf("Le voisin recu est inexistant\n");
		return;
    }

    // Le voisin est valide, on l'ajoute a la table des voisins
    Voisin* v = voisin_create(address, sizeof(struct sockaddr_in6), FALSE); 
    
    // On affiche le voisin
    char * n = udp_sockaddr_to_char(v->addr, &error);
    printf("NOUVEAU VOISIN : %s\n", n);
    free(n);
    
    // On ajoute le nouveau voisin a la table
    if(!table_add_voisin(tv, v, time(NULL))) {
		// Si l'ajoute n'a pas pu se faire, on detruis le voisin
		voisin_destroy(v);
    }

    free(name);
}

/*Traitement d'une Node State Request.
info contient l'id d'un node. ajoute ce node (s'il existe) a la reponse du traitement.
S'il n'existe pas, un warning est ajoute a la reponse du traitement.*/
void receive_node_state_request(traitement* trait, char* info, table* t){  
    printf("On recoit un node state request\n");
    // On recupere le node
    noeud *n = get_node(t, *((uint64_t*)info));
    if ( n == NULL ){
		// S'il n'existe pas, on envoie un Warning
		send_warning(trait, "Alors ca te plait d'envoyer des node state request sans node hash de ma part en amont espèce de moule a gaufre");
		return;
    }
    // S'il existe, on l'ajoute a la reponse
    send_node_state(trait, n);
}

/*Traitement d'une Network Hash.
info contient un hash du reseau. On recalcul notre hash du reseau,
puis on le compare avec le hash contenu dans info.
Si il ne sont pas les meme, on ajoute une Network State Request a la reponse du traitement.*/
void receive_network_hash(traitement* trait, char* info, table* t){
    printf("On recoit un network hash\n");
    if ((t->ajour) != 1 ) {
		// On met a jour le notre hash du reseau
		network_hash(t);
    }
    if(memcmp(t->hash, info, 16) != 0) {
		// notre hash n'est pas a jour,
		// on ajoute une Network State Request a la reponse du traitement
		send_network_state_request(trait);
    }
}

/*Traitement d'une Network State Request.
On ajoute chacun des node de notre table dans la reponse du traitement.*/
void receive_network_state_request(traitement* trait, table* t){
    printf("On recoit un network state request\n");
    int i = 0;
    noeud* n = t->first ;
    // On ajoute tous les noeuds a la reponse
    while ( n != NULL ){
		i++;
		send_node_hash(trait, n);
		n = n->suivant;
    }
}

/*Traitement d'une Node Hash.
info contient un noeud (juste id et numero de seq).
- Si ce noeud n'est pas dans notre table,
on demande le noeud entier en ajoutant une Node State Request a la reponse.
- Si notre seq pour ce noeud est plus grande, on ajoute une Node State a la response.
- Si le seq de info est plus grand, on ajoute une Node State Request a la reponse.
- Si les deux ne sont pas egaux, on ajoute une Node State Request a la reponse.*/
void receive_node_hash(traitement* trait, char* info, table* t){
    printf("On recoit un node hash\n");
    uint64_t* addr = (uint64_t*)info;
    uint16_t* seq = (uint16_t*)(&(info[8]));
    noeud* n = get_node(t, *addr);
    if ( n == NULL ){
		send_node_state_request(trait, addr);
		return;
    }
    if (sup_strict(*seq, n->sequence)){
		send_node_state_request(trait, addr);
		return;
    }
    if (sup_strict(n->sequence, *seq)){
		send_node_state(trait, n);
		return;
    }
    if ( (memcmp(info, (n->hash), 16)) != 0 ) {
      	send_node_state_request(trait, addr);
    }
}

/*Traitement d'une Node State.
info contient un noeud complet. on met a jour ce le noeud de meme id
que celui de info dans notre table.*/
void receive_node_state(traitement* trait, char* info, table* t, int len){
    printf("On recoit un node state de façon basique\n");
    uint64_t* addr = (uint64_t*)info;
    uint16_t* seq = (uint16_t*)(&info[8]);
    noeud new = {.id = *addr, .sequence = *seq, .donnee = "", .suivant = NULL};
    memmove( &(new.donnee), &(info[26]), len-26);
    memmove( &(new.hash), &(info[10]), 16);
    noeud* n = get_node(t, *addr);

    //Connait on deja une version de ce noeud
    if ( n == NULL ) {
		actualiser(t, &new);
		return;
    }

    //Ce noeud est-il différent de celui qu'on connait
    if (memcmp(new.hash, n->hash, 16) != 0 ){
		//Est ce le mien
		if (*addr == ID){
			//Est ce un piratage
			if ( sup_or_equal((new.sequence), (n->sequence))) 
				(n->sequence) = htons( (1+ntohs(new.sequence)) & 65535); 
			return;
		}
		//Lequel est le plus récent
		if (sup_strict(n->sequence, *seq)) {
			send_node_state(trait,n);
			return;
		}
		actualiser(t, &new);
    }
}

void receive_node_state_intermediaire(traitement* trait, char* info, table* t, int len){
    printf("On recoit un node state de façon\n");
    uint64_t* addr = (uint64_t*)info;
    uint16_t* seq = (uint16_t*)(&info[8]);
    noeud new = {.id = *addr, .sequence = *seq, .donnee = "", .suivant = NULL};
    memmove( &(new.donnee), &(info[26]), len-26);
    memmove( &(new.hash), &(info[10]), 16);
    noeud* n = get_node(t, *addr);
    // Connait on deja une version de ce noeud
    if ( n == NULL ) {
		actualiser(t, &new);
		return;
    }
    // Ce noeud est-il différent de celui qu'on connait
    if (memcmp(new.hash, n->hash, 16) != 0 ){
		hash_node(&new);
		if (memcmp(new.hash, &(info[10]), 16) != 0 ){
			send_warning(trait, "node sate incohérent dans son hachage");
			return;
		}
		// Est ce le mien
		if (*addr == ID){
			// Est ce un piratage
			if ( sup_or_equal((new.sequence), (n->sequence))) 
				(n->sequence) = htons( (1+ntohs(new.sequence)) & 65535); 
			return;
		}
		// Lequel est le plus récent
		if (sup_strict(n->sequence, *seq)) {
			send_node_state(trait,n);
			return;
		}
		actualiser(t, &new);
    }
}

void receive_node_state_protected(traitement* trait, char* info, table* t, int len){
    printf("On recoit un node state de façon\n");
    uint64_t* addr = (uint64_t*)info;
    uint16_t* seq = (uint16_t*)(&info[8]);
    noeud new = {.id = *addr, .sequence = *seq, .donnee = "", .suivant = NULL};
    memmove( &(new.donnee), &(info[26]), len-26);
    memmove( &(new.hash), &(info[10]), 16);
    noeud* n = get_node(t, *addr);
    
    //Contrôle de cohérence du hash
    hash_node(&new);
    if (memcmp(new.hash, &(info[10]), 16) != 0 ){
		send_warning(trait, "node sate incohérent dans son hachage");
		return;
    }
    
    //Connait on deja une version de ce noeud
    if ( n == NULL ) {
		actualiser(t, &new);
		return;
    }
    
    //Ce noeud est-il différent de celui qu'on connait
    if (memcmp(new.hash, n->hash, 16) != 0 ){
		//Est ce le mien
		if (*addr == ID){
			//Est ce un piratage
			if ( sup_or_equal((new.sequence), (n->sequence))) 
				(n->sequence) = htons( (1+ntohs(new.sequence)) & 65535); 
			return;
		}
		//Lequel est le plus récent
		if (sup_strict(n->sequence, *seq)) {
			send_node_state(trait,n);
			return;
		}
		actualiser(t, &new);
    }
}

/*Traitement d'un Warning.
Affiche le contenu info. et le stocke dans le fichier*/
void receive_warn(char* info, int k){
    printf("On recoit un warning\n");
    FILE * fp;
    fp = fopen (WARNING_LOCATION, "a");
    fprintf(fp, "\n\n\n");
    for (int i=0; i<k;i++)
      	fprintf(fp, "%c",info[i]);  
    fclose(fp);
}

/*Ajoute une TLV Pad1 a la reponse du traitement*/
void send_pad1(traitement* trait){
    printf("On envoie un pad1\n");
    int i = get_indice(trait, 1);
    if ( i != -1 ){
		requete * req = &((trait->reponse)[i]);
		char tlv=0;
		(req->body)[req->length] = tlv;
		(req->length) += 1;
    }
}

/*Ajoute une TLV PadN de taille k a la reponse du traitement*/
void send_padN(traitement* trait, int k){
    printf("On envoie un padN\n");
    int i = get_indice(trait, 2+k);
    if ( i != -1 ){
		requete * req = &((trait->reponse)[i]);
		char tlv = 1;
		char len = k;
		(req->body)[req->length] = tlv;
		(req->body)[(req->length)+1] = len;
		memset( &(req->body)[(req->length)+2], 0, k);
		(req->length) += (k+2);
    }
}

/*Ajoute une TLV Neighbour Request a la reponse du traitement*/
void send_neighbour_request(traitement* trait){
    printf("On envoie un neighbour request\n");
    int i = get_indice(trait, 2);
    if ( i != -1 ){
		requete * req = &((trait->reponse)[i]);
		char tlv = 2;
		char len = 0;
		(req->body)[req->length] = tlv;
		(req->body)[(req->length)+1] = len;
		(req->length) += 2;
    }
}

/*Ajoute une TLV Neigbhour contenant un voisin aleatoire de la table des voisin tv
 a la reponse du traitement*/
void send_neighbour(traitement* trait, TableVoisins* tv){
	// On recupere l'ip et le port d'un voisin aleatoire
    IpPort ip = table_get_random_ip_port(tv);
    printf("SEND NEIGHBOUR : %s | %d\n", ip.ip, ntohs(ip.port));
    
	// On ajoute le Neighbour contenant ces ip et port
	int i = get_indice(trait, 20);
    if ( i != -1 ){
		// On rempli l'en tete du tlv
		requete * req = &((trait->reponse)[i]);
		char tlv = 3;
		char len = 18;
		(req->body)[req->length] = tlv;
		(req->body)[(req->length)+1] = len;

		memset(req->body+req->length+2, 0, 18);
		memcpy(req->body+req->length+2, ip.ip, 16);
		memcpy(req->body+req->length+18, &(ip.port), 2);
		(req->length) += 20;
    }
}

/*Ajoute une TLV Network Hash contenant le hash recalcule de notre table si elle n'est pas a jour
 a la reponse du traitement*/
void send_network_hash(traitement* trait, table* t){
    printf("On envoie un network hash\n");
    int i = get_indice(trait, 18);
    if ( i != -1 ){
		requete * req = &((trait->reponse)[i]);
		char tlv = 4;
		char len = 16;
		(req->body)[req->length] = tlv;
		(req->body)[(req->length)+1] = len;
		if ( t->ajour != 1 ) {
			// Si on n'est pas a jour, on recalcul notre hash
			network_hash(t);
		}
		memmove( &(req->body)[(req->length)+2], (t->hash), 16);
		(req->length) += 18;
    }
}

/*Ajoute une TLV Network State Request a la reponse du traitement*/
void send_network_state_request(traitement* trait){
    printf("On envoie un network state request\n");
    int i = get_indice(trait, 2);
    if ( i != -1 ){
		requete * req = &((trait->reponse)[i]);
		char tlv = 5;
		char len = 0;
		(req->body)[req->length] = tlv;
		(req->body)[(req->length)+1] = len;
		(req->length) += 2;
    }
}

/*Ajoute une TLV Node Hash contenant le id, le seq
et le hash du noeud n a la reponse du traitement*/
void send_node_hash(traitement* trait, noeud* n){
    printf("On envoie un node hash\n");
    int i = get_indice(trait, 28);
    if ( i != -1 ){
		requete * req = &((trait->reponse)[i]);
		char tlv = 6;
		char len = 26;
		(req->body)[req->length] = tlv;
		(req->body)[(req->length)+1] = len;
		memmove( &(req->body)[(req->length)+2], n, 10);
		memmove( &(req->body)[(req->length)+12], (n->hash), 16);
		(req->length) += 28;
    }
}

/*Ajoute une TLV Node State Request a la reponse du traitement*/
void send_node_state_request(traitement* trait, uint64_t* id){
    printf("On envoie un node state request\n");
    int i = get_indice(trait, 10);
    if ( i != -1 ){
		requete * req = &((trait->reponse)[i]);
		char tlv = 7;
		char len = 8;
		(req->body)[req->length] = tlv;
		(req->body)[(req->length)+1] = len;
		memmove( &(req->body)[(req->length)+2], id, 8);
		(req->length) += 10;
    }
}

/*Ajoute une TLV Node State contenant toute les information du noeud n
 a la reponse du traitement*/
void send_node_state(traitement* trait, noeud* n){
    int l = strlen(n->donnee);
    int i = get_indice(trait, l+28);
    if ( i != -1 ){
		requete * req = &((trait->reponse)[i]);
		char tlv = 8;
		char len = (26+l);
		(req->body)[req->length] = tlv;
		(req->body)[(req->length)+1] = len;
		memmove( &(req->body)[(req->length)+2], n, 10);
		memmove( &(req->body)[(req->length)+12], (n->hash), 16);
		memmove( &(req->body)[(req->length)+28], (n->donnee), l);
		(req->length) += (28+l);
    }
}

/*Ajoute une TLV TLV Warning contenant le message info
 a la reponse du traitement*/
void send_warning(traitement* trait, char* info){
    printf("On envoie un warning\n");
    int l = (int) strlen(info);
    int i = get_indice(trait, l+2);
    if ( i != -1 ){
		requete * req = &((trait->reponse)[i]);
		char tlv = 9;
		char len = l;
		(req->body)[req->length] = tlv;
		(req->body)[(req->length)+1] = len;
		memmove( &(req->body)[(req->length)+2], info, l);
		(req->length) += (l+2);
    }
}

/*Initie le traitement des TLV contenus dans la requete question du traitement*/
traitement* traitement_general(traitement* trait, table* t, TableVoisins* tv){
    printf("TRAITEMENT GENERAL\n");
    requete* req= trait->question;
	// On initialise la tete de lecture des requetes
	// Et le nombre de requetes traites
    trait->head = 0;
    trait->nbrep = 0;

	// On traite chaque requete une par une
    while ( req->length > 1 ) {
		printf("requete size = %d\n", req->length);
		traitement_TLV(trait, t, tv);
    }
    printf("on sort du traitement");
    return trait;
}

/*Fait les actions necessaire pour la requete courante du traitement*/
void traitement_TLV(traitement* trait, table* t, TableVoisins* tv){
    printf("Traitement TLV head = %d\n", trait->head);
    requete* req = trait->question;

	//longueur restante theorique
    int len = req->length;
	// longueur du tlv
    int lenTLV;
	// k vaut le numero de tlv
    int k = (req->body)[trait->head];

    switch(k){      
	//e TLV est une Pad1
    case 0:
		printf("TLV PAD1\n");
		// Rien a faire, on passe a au TLV suivante
		req->length -= 1;
		(trait->head) += 1;
		break;
	//le TLV est une PadN
    case 1:
		printf("TLV PADN\n");
		lenTLV = (req->body)[(trait->head)+ 1];
		if ( lenTLV > len-1 ){
			req->length = 0;
			send_warning(trait, "padN qui depasse de la requete");
			break;
		}
		req->length -= (lenTLV+2);
		(trait->head) += (lenTLV+2); break;
	//le TLV est une Neighbour Request
    case 2:
		printf("TLV TLV Neighbour request\n");
		req->length -= 2;
		(trait->head) += 2;
		receive_neighbour_request(trait, tv); break;
	//le TLV est une Neighbour
    case 3:
		printf("TLV NEIGHBOUR\n");
		lenTLV = (req->body)[(trait->head)+ 1];
		if ( lenTLV > len-1 || lenTLV != 18){
			req->length = 0;
			send_warning(trait, "un neighbour que tu m'as envoyé depassait du paquet ou bien length different de 18, bizarre");
			break;
		}
		req->length -= 20;
		receive_neighbour(trait, (req->body)+(trait->head)+2, tv);
		(trait->head) += 20; break;
	//le TLV est une Network Hash
    case 4:
		printf("TLV Network hash\n");
		lenTLV = (req->body)[(trait->head)+ 1];
		if ( lenTLV > len-1 || lenTLV != 16){
			req->length = 0;
			send_warning(trait, "un network hash que tu m'as envoyé depassait du paquet ou bien length different de 16, bizarre");
			break;
		}
		req->length -= 18;
		receive_network_hash(trait, (req->body)+(trait->head)+2, t);
		(trait->head) += 18; break;
	//le TLV est une Network State Request
    case 5:
		printf("TLV Network State Request\n");
		lenTLV = (req->body)[(trait->head)+ 1];
		if (lenTLV != 0){
			req->length = 0;
			send_warning(trait, "un network state request que tu m'as envoyé depassait du paquet ou avait un length different de 0, bizarre");
			break;
		}
		req->length -= 2;
		(trait->head) += 2;
		receive_network_state_request(trait, t); break;
	//le TLV est une Node Hash
    case 6:
		printf("TLV Node hash\n");
		lenTLV = (req->body)[(trait->head)+ 1];
		if ( lenTLV > len-1 || lenTLV != 26 ){
			req->length = 0;
			send_warning(trait, "un node state que tu m'as envoyé depassait du paquet ou taille invalide, bizarre");
			break;
		}
		req->length -= 28;
		receive_node_hash(trait, (req->body)+(trait->head)+2, t);
		(trait->head) += 28;
		printf("ici\n");break;
	//le TLV est une Node State Request
    case 7:
		printf("TLV Node State Request\n");
		lenTLV = (req->body)[(trait->head)+ 1];
		if (lenTLV > len-1 || lenTLV != 8 ){
			req->length = 0;
			send_warning(trait, "un node state request que tu m'as envoyé depassait du paquet ou taille invalide");
			break;
		}
		req->length -= 10;
		receive_node_state_request(trait, (req->body)+(trait->head)+2, t);
		(trait->head) += 10; break;
	//le TLV est une Node State
    case 8:
		printf("TLV Node State");
		lenTLV = (req->body)[(trait->head)+ 1];
		if (lenTLV > len-1 || lenTLV < 26 || lenTLV > 218){
			req->length = 0;
			send_warning(trait, "un node state que tu m'as envoyé depassait du paquet ou taille non valide, bizarre");
			break;
		}
		(req->length) -= (lenTLV+2);
		receive_node_state(trait, (req->body)+(trait->head)+2, t, lenTLV);
		(trait->head) += (lenTLV+2); break;
	//le TLV est un Warning
    case 9:
		printf("TLV Warning\n");
		lenTLV = (req->body)[(trait->head)+ 1];
		if (lenTLV > len-1 ){
			req->length = 0;
			send_warning(trait, "un warning que tu m'as envoyé depassait du paquet ou taille insuffisante, bizarre");
			break;
		}
		(req->length) -= (lenTLV+2);
		receive_warn((req->body)+(trait->head)+2, len);
		(trait->head) += (lenTLV+2); break;

	//le TLV est un TLV non implémentés
    default:
		printf("UNKNOWN TLV\n");
		send_warning(trait, "TLV non implémenté dans notre version, il sert à quoi ce tlv");
		lenTLV = (req->body)[(trait->head)+ 1];
		if (lenTLV > len-1){
			req->length = 0;
			break;
		}
		req->length -= (lenTLV+1);
		(trait->head) += (len+1); break;
    }
}
