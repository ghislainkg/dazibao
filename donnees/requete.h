#ifndef _REQUETE_
#define _REQUETE_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>

#include "rfc6234-master/sha.h"
#include "../udp/data/table.h"
#include "../udp/cudp/udpbase.h"

#define ID 0xABCDEABCDEABCDEF
#define TAILLE_MALLOC 400000

#define NB_REQUETE_REPONSE 12
#define TAILLE_DATAGRAMME  1020

/*Fichier dans lequel sont stockes les warnings recus*/
#define WARNING_LOCATION "files/warning.txt"

/*Definition d'une requete*/
typedef struct requete{
  uint16_t length;
  char body[TAILLE_DATAGRAMME];
} requete;

typedef union{
  intmax_t a;
  void *adr;
  long double c;
}align_data;

/*Definition d'un noeud de la table de donnees*/
typedef struct noeud{
  uint64_t id;
  uint16_t sequence; // codé en gros boutiste
  char donnee[193]; //192 de message plus la sentinelle 
  struct noeud* suivant;
  unsigned char hash[17];
  align_data data[];
}noeud;

/*La table de donnees*/
typedef struct table{
  noeud* first; // Le premier noeud de la table
  void* free;
  unsigned char hash[16]; // Notre hash du reseau
  int nb_noeuds;
  int ajour; // Indique si on a fait une demande Node State Request
}table;

/*Le traitement est constitu une requete et sa reponse*/
typedef struct traitement{
  requete* question; // La requete
  requete reponse[NB_REQUETE_REPONSE]; // La reponse qui peut contenir plus d'une requete
  uint32_t nbrep; // Le nombre de reponse
  int head; //tete de lecture de la question
} traitement;


/*Definissons d'abord les fonctions qui permette de traiter chaque type de requete*/

/*Traitement d'une Neighbour request.
Ajoute a la reponse du traitement 
l'adresse ip et le port d'un voisin aleatoire de la table des voisins tv*/
void receive_neighbour_request(traitement* trait, TableVoisins* tv);

/*Traitement d'une Neighbour.
info doit contenir une adresse ip et un port a la suite.
Un voisin est cree et ajoute a la table des voisin a partir de ces ip et port*/
void receive_neighbour(traitement* trait, char* info, TableVoisins* tv);

/*Traitement d'une Node State Request.
info contient l'id d'un node. ajoute ce node (s'il existe) a la reponse du traitement.
S'il n'existe pas, un warning est ajoute a la reponse du traitement.*/
void receive_node_state_request(traitement* trait, char* info, table* t);

/*Traitement d'une Network Hash.
info contient un hash du reseau. On recalcul notre hash du reseau,
puis on le compare avec le hash contenu dans info.
Si il ne sont pas les meme, on ajoute une Network State Request a la reponse du traitement.*/
void receive_network_hash(traitement* trait, char* info, table* t);

/*Traitement d'une Network State Request.
On ajoute chacun des node de notre table dans la reponse du traitement.*/
void receive_network_state_request(traitement* trait, table* t);

/*Traitement d'une Node Hash.
info contient un noeud (juste id et numero de seq).
- Si ce noeud n'est pas dans notre table,
on demande le noeud entier en ajoutant une Node State Request a la reponse.
- Si notre seq pour ce noeud est plus grande, on ajoute une Node State a la response.
- Si le seq de info est plus grand, on ajoute une Node State Request a la reponse.
- Si les deux ne sont pas egaux, on ajoute une Node State Request a la reponse.*/
void receive_node_hash(traitement* trait, char* info, table* t);

/*Traitement d'une Node State.
info contient un noeud complet. on met a jour ce le noeud de meme id
que celui de info dans notre table.*/
void receive_node_state(traitement* trait, char* info, table* t, int len);

void receive_node_state_intermediaire(traitement* trait, char* info, table* t, int len);
void receive_node_state_protected(traitement* trait, char* info, table* t, int len);

/*Traitement d'un Warning.
Affiche le contenu info. et le stocke dans le fichier*/
void receive_warn(char* info, int k);

/*Definissons les fonctions qui permettent d'ajouter une requete particuliere a la reponse du traitement*/

/*Ajoute une TLV Pad1 a la reponse du traitement*/
void send_pad1(traitement* trait);

/*Ajoute une TLV PadN de taille k a la reponse du traitement*/
void send_padN(traitement* trait, int k);

/*Ajoute une TLV Neighbour Request a la reponse du traitement*/
void send_neighbour_request(traitement* trait);

/*Ajoute une TLV Neigbhour contenant un voisin aleatoire de la table des voisin tv
 a la reponse du traitement*/
void send_neighbour(traitement* trait, TableVoisins* tv);

/*Ajoute une TLV Network Hash contenant le hash recalcule de notre table si elle n'est pas a jour
 a la reponse du traitement*/
void send_network_hash(traitement* trait, table* t);

/*Ajoute une TLV Network State Request a la reponse du traitement*/
void send_network_state_request(traitement* trait);

/*Ajoute une TLV Node Hash contenant le id, le seq
et le hash du noeud n a la reponse du traitement*/
void send_node_hash(traitement* trait, noeud* n);

/*Ajoute une TLV Node State Request a la reponse du traitement*/
void send_node_state_request(traitement* trait, uint64_t* id);

/*Ajoute une TLV Node State contenant toute les information du noeud n
 a la reponse du traitement*/
void send_node_state(traitement* trait, noeud* n);

/*Ajoute une TLV Warning contenant le message info
 a la reponse du traitement*/
void send_warning(traitement* trait, char* info);


/*Initie le traitement des requetes (question) du traitement*/
traitement* traitement_general(traitement* trait, table* t, TableVoisins* tv);

/*Fait les actions necessaire pour la requete courante du traitement*/
void traitement_TLV(traitement* trait, table* t, TableVoisins* tv);

/*Retourne une Neighbour request*/
requete * neighbour_request();

/*Retourne une Network State Request*/
requete * network_state_request();

/*Retourne une Network Hash contenant le hash de notre table,
apres l'avoir recalculer si nptre table n'est pas a jour*/
requete* network_hash_send(table* t);

/*Petmet d'ajouter le noeud n a la table s'il n'y est pas encore,
et le mettre a jour s'il y est deja. t->ajour est mis a vrai*/
void actualiser(table* t, noeud* n);

/*Fait un hash de str (qui est de taille len), et l'ecris dans adr.
retourne 1 si tout se passe bien
et -1 en cas d'erreur.*/
int h(const char* s, int k, char* adr );

/*Calcul le hash de la donnee du noeud et le met a jour*/
void hash_node(noeud* n);

/*Calcul et met a jour le hash du reseau dans la table t*/
void network_hash(table* t);

/*Affiche un noeud*/
void print(noeud* n);

/*Retourne le noeud de la table t dont l'id est passe en parametre.
Retourne NULL s'il n'existe pas*/
noeud* get_node(table* t, uint64_t id);

#endif
