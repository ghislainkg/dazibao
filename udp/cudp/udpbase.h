#ifndef _UTILS_
#define _UTILS_

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <net/if.h>

#include "message.h"

#include "error.h"

typedef enum WaitType
{
	WRITE,
	READ
} WaitType;

#define MULTICAST_INTERFACE_NAME "wlo1"
#define MULTICAST_ADDR "ff12::25ea:32da:1de2:ab45"
#define MULTICAST_PORT 8080

/* Genere la sockaddr_in6associee aux nom d'hote et port
 avec la bonne famille d'adresse*/
void udp_get_sockaddr(const char * hostname, const char * port,
	struct sockaddr_in6* addr, Error * error);

/*La meme chose que udp_get_sockaddr sauf que le port est un entier*/
void udp_get_sockaddr_portnumber(const char * hostname, uint16_t port,
	struct sockaddr_in6* addr, Error * error);

/* Retourne un nouveau descripteur de fichier pour un socket de famille family
(devrai etre celle retourne par getSockAddr)
Ce descripteur de fichier est inscrit au groupe multicast d'adresse multicastAddress,
 de port multicastPort et via l'interfacemulticastInterface
 pour utiliser les valeur par defaut (definie ci dessus) :
 - multicastAddress = NULL
 - multicastPort <= 0
 - multicastInterface = NULL*/
int udp_get_socketfd(char * multicastAddress, int multicastPort, char * multicastInterface, Error * error);

/*Bind la socket au port et interface passe en parrametre*/
void udp_bind_socket_to_port(int socketfd, char * interface, int family, const char *port, Error * error);

/*Envoi le buffer du Message a l'adresse du message via le socket du message*/
void udp_send(Message * message, Error * error);

/*L'adresse de message sera changer en l'adresse de multicast*/
void udp_multicast_send(Message * message, char * multicastAddress, int multicastPort, char * multicastInterface, Error * error);

/*Recoit un message via le socket du Message,
ecris le resultat dans le buffer du message et
l'adresse de l;emeteur dans l'adresse du message*/
void udp_receive(Message * message, Error * error);

/*Attends pendant second secondes et millis millisecondes que l'on puisse lire ou ecrire
dans le socket du Message*/
int udp_wait_message(const Message * message, WaitType type, int second, int millis);

/*Retourne une chaine de caracteres contenant l'ip et le port*/
char * udp_sockaddr_to_char(const struct sockaddr_in6 * addr, Error * error);

/*Ecris dans *ip et *port l'adresse ip et le port de addr*/
void udp_sockaddr_to_char_split(const struct sockaddr_in6 * addr, char ** ip, char ** port, Error * error);

/*Verifi si deux adresse son les meme*/
int udp_address_equal(const struct sockaddr_in6* addr1, unsigned int size1,
	const struct sockaddr_in6* addr2, unsigned int size2, Error * error);

#endif