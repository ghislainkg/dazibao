#ifndef _ERROR_
#define _ERROR_

typedef enum Error
{
	NO,
	
	// general
	NULL_POINTER,

	// udp errors
	FAIL_SOCKADDR,
	FAIL_SOCKETFD,
	FAIL_SEND,
	FAIL_RECEIVE,
	FAIL_BIND,
	FAIL_GETTING_ADDR,

	// Message functions related errors
	HEADER_NOT_SET,
	MSG_REACH_CAPACITY,
	INVALID_MAGIC_VEC,
	INVALID_VERSION_VEC,
	INVALID_LENGTH_VEC
} Error;

// Verifi s'il y a une erreur
#define ERROR(errorval) \
		errorval != NO

// donne val comme valeur a *error si error n'est pas null
#define SET_ERROR(error, val) \
	if(error != NULL) \
		*error = val;;

// verifi si pointer est null et dans le cas echeant,
// donne pour valeur NULL_POINTER a error si erreur n'est pas null
// et fait retourner returnVal
#define CHECK_NULL_POINTER(error, pointer, returnVal) \
	if(pointer == NULL) { \
		SET_ERROR(error, NULL_POINTER); \
		return returnVal; \
	};

#define DEBUG 0
#define FOR_DEBUG(action) \
	if(DEBUG) { \
		action \
	};

#endif