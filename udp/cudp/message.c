#include "message.h"

/* Creation d'un nouveau message sans formater sont contenu.
Fait une copie de addr*/
Message * message_create(const struct sockaddr_in6 * addr, int socketfd, unsigned int attribCapacity) {
	struct msghdr * content = malloc(sizeof(struct msghdr));

	if(addr != NULL) {
		size_t len = sizeof(const struct sockaddr_in6);
		struct sockaddr_in6 * copy = malloc(len);
		memcpy(copy, addr, len);
		content->msg_name = copy;
		content->msg_namelen = len;
	}
	else {
		content->msg_name = NULL;
		content->msg_namelen = 0;
	}

	// On donne d'abord assez de place pour le header	
	content->msg_iov = malloc((HEADER_SIZE + attribCapacity)*sizeof(struct iovec) );
	content->msg_iovlen = 0; // Le nombre de iovec effectivement occupes
	
	content->msg_control = NULL;
	content->msg_controllen = 0;
	content->msg_flags = 0;

	Message message;
	message.socketfd = socketfd;
	message.content = content;
	
	Message * res = malloc(sizeof(Message));
	*res = message;
	return res;
}

/*Change l'adresse d'un message, libere la memoire de l'adresse precedente*/
void message_set_addr(Message * message, struct sockaddr_in6 *addr) {
	free(message->content->msg_name);
	if(addr != NULL) {
		size_t len = sizeof(const struct sockaddr_in6);
		struct sockaddr_in6 * copy = malloc(len);
		memcpy(copy, addr, len);
		message->content->msg_name = copy;
		message->content->msg_namelen = len;
	}
	else {
		message->content->msg_name = NULL;
		message->content->msg_namelen = 0;
	}
}

/* Creation d'un nouveau message en formatant les champ de content pour recevoir l'en-tete d'un message.
Fait une copie de addr*/
Message * message_create_for_receive(
	const struct sockaddr_in6 * addr,
	int socketfd,
	unsigned int attribCapacity, unsigned int attribSize) {
	
	struct msghdr * content = malloc(sizeof(struct msghdr));

	if(addr != NULL) {
		size_t len = sizeof(const struct sockaddr_in6);
		struct sockaddr_in6 * copy = malloc(len);
		memcpy(copy, addr, len);
		content->msg_name = copy;
		content->msg_namelen = len;
	}
	else {
		content->msg_name = NULL;
		content->msg_namelen = 0;
	}

	// On donne d'abord assez de place pour le header
	content->msg_iov = malloc((HEADER_SIZE + attribCapacity)*sizeof(struct iovec) );
	content->msg_iovlen = attribCapacity; // Le nombre de iovec effectivement occupes

	// Ensuite on cree de la place pour les attributs du header
	struct iovec vec;
	// Le magic
	vec.iov_base = malloc(1);
	vec.iov_len = 1;
	content->msg_iov[0] = vec;
	// La version
	vec.iov_base = malloc(1);
	vec.iov_len = 1;
	content->msg_iov[1] = vec;
	// La body length
	vec.iov_base = malloc(2);
	vec.iov_len = 2;
	content->msg_iov[2] = vec;

	// Ensuite le reste des attributs avec le format demande
	for (int i = 3; i < attribCapacity; ++i) {
		struct iovec vec;
		vec.iov_base = malloc(attribSize);
		vec.iov_len = attribSize;
		content->msg_iov[i] = vec;
	}
	
	content->msg_control = NULL;
	content->msg_controllen = 0;
	content->msg_flags = 0;

	Message message;
	message.socketfd = socketfd;
	message.content = content;
	
	Message * res = malloc(sizeof(Message));
	*res = message;
	return res;
}

void message_init(Message * message) {
	struct msghdr * content = message->content;
	for (int i = 0; i < content->msg_iovlen; ++i) {
		void * vec  = content->msg_iov[i].iov_base;
		free(vec);
		content->msg_iov[i].iov_base = NULL;
	}
	content->msg_iovlen = 0;
}

// detrui un message en profondeur
void message_destroy(Message * message) {
	struct msghdr * content = message->content;
	// On supprime l'addresse
	free(content->msg_name);
	// On supprime chaque vecteur
	for (int i = 0; i < content->msg_iovlen; ++i) {
		void * vec  = content->msg_iov[i].iov_base;
		free(vec);
	}
	// On supprime le tableau de vecteurs
	free(content->msg_iov);
	// On supprime le msghdr
	free(content);
	// Et enfin on supprime le message
	free(message);
}

// Fixe les valeurs du header de message
// Doit etre appelle avant d'ajouter des attributs dans le body
void message_set_header(Message * message, char magic, char version, uint16_t length, Error * error) {
	SET_ERROR(error, NO)
	CHECK_NULL_POINTER(error, message, )

	struct iovec magic_vec;
	magic_vec.iov_base = malloc(1);
	magic_vec.iov_len = 1;
	*((char*)magic_vec.iov_base) = magic;

	message->content->msg_iov[message->content->msg_iovlen++] = magic_vec;

	struct iovec version_vec;
	version_vec.iov_base = malloc(1);
	version_vec.iov_len = 1;
	*((char*)version_vec.iov_base) = version;

	message->content->msg_iov[message->content->msg_iovlen++] = version_vec;

	length = htons(length);
	struct iovec length_vec;
	length_vec.iov_base = malloc(2);
	length_vec.iov_len = 2;
	memmove(length_vec.iov_base, &length, 2);

	message->content->msg_iov[message->content->msg_iovlen++] = length_vec;
}

// Ajoute un iovec au message
// Renvoie une erreur s'il n'y a plus de place ou s'il n'y a pas de header
void message_add_attribut(Message * message, struct iovec attrib, Error * error) {
	SET_ERROR(error, NO)
	CHECK_NULL_POINTER(error, message, )
	// On teste s'il y a by un header
	if(message->content->msg_iovlen < HEADER_SIZE) {
		SET_ERROR(error, HEADER_NOT_SET)
		return;
	}
	// On teste s'il y a assez de place
	if(message->attribCapacity <= message->content->msg_iovlen) {
		SET_ERROR(error, MSG_REACH_CAPACITY)
		return;
	}

	message->content->msg_iov[message->content->msg_iovlen++] = attrib;
}

// Ajoute nbrAttribs iovec au message
// Renvoie une erreur s'il n'y a plus de place ou s'il n'y a pas de header
void message_add_attributs(Message * message, struct iovec * attribs, size_t nbrAttribs, Error * error) {
	SET_ERROR(error, NO)
	CHECK_NULL_POINTER(error, message,)
	CHECK_NULL_POINTER(error, attribs,)

	for (int i = 0; i < nbrAttribs; ++i){
		struct iovec attrib = attribs[i];
		message_add_attribut(message, attrib, error);
		if(ERROR(*error))
			return;
	}
}

// Ajoute les iovec contenu dans 'content' au message
// Renvoie une erreur s'il n'y a plus de place ou s'il n'y a pas de header
void message_add_attributs_from_content(Message * message, struct msghdr * content, Error * error) {
	SET_ERROR(error, NO)
	CHECK_NULL_POINTER(error, message,)
	CHECK_NULL_POINTER(error, content,)

	struct iovec * attribs = content->msg_iov;
	message_add_attributs(message, attribs, content->msg_iovlen, error);
}

// Retourne true si le magic du header du message a pour valeur CORRECT_MAGIC
// Et false sinon
// Donne une erreur s'il n'y a pas encore de magic
int message_is_magic_correct(Message * message, Error * error) {
	SET_ERROR(error, NO)
	CHECK_NULL_POINTER(error, message, 0)

	char magic = message_get_magic(message, error);
	if(ERROR(*error))
		return 0;
	if(magic == CORRECT_MAGIC)
		return 1;
	else
		return 0;
}

// Retourne la valeur du magic du header de message
// Donne une erreur s'il n'y a pas encore de magic
char message_get_magic(Message * message, Error * error) {
	SET_ERROR(error, NO)
	CHECK_NULL_POINTER(error, message, 0)

	size_t len = message->content->msg_iov[MAGIC_INDEX].iov_len;
	if(len != 1) {
		SET_ERROR(error, INVALID_MAGIC_VEC);
		return 0;
	}
	char * res = (char*)message->content->msg_iov[MAGIC_INDEX].iov_base;
	return res[0];
}

// Retourne la valeur de la version du header de message
// Donne une erreur s'il n'y a pas encore de version
char message_get_version(Message * message, Error * error) {
	SET_ERROR(error, NO)
	CHECK_NULL_POINTER(error, message, 0)

	size_t len = message->content->msg_iov[VERSION_INDEX].iov_len;
	if(len != 1) {
		SET_ERROR(error, INVALID_VERSION_VEC)
		return 0;
	}
	char * res = (char*)message->content->msg_iov[VERSION_INDEX].iov_base;
	return res[0];
}

// Retourne la valeur de la taille du body contenu dans le header de message
// Donne une erreur s'il n'y a pas encore de length du body
uint16_t message_get_body_len(Message * message, Error * error) {
	SET_ERROR(error, NO)
	CHECK_NULL_POINTER(error, message, 0)

	size_t len = message->content->msg_iov[LENGTH_INDEX].iov_len;
	if(len != 2) {
		SET_ERROR(error, INVALID_LENGTH_VEC)
		return 0;
	}
	uint16_t res;
	memmove(&res, message->content->msg_iov[LENGTH_INDEX].iov_base, 2);
	return ntohs(res);///256;
}

// Retourne dans la valeur pointee par body (sa valeur est un pointer)
// la liste des iovec composent le body du message
void message_get_body(Message * message, struct iovec ** body, size_t * countBodyVec, Error * error) {
	SET_ERROR(error, NO)
	CHECK_NULL_POINTER(error, message,)
	CHECK_NULL_POINTER(error, body,)
	CHECK_NULL_POINTER(error, countBodyVec,)

	*countBodyVec = message->content->msg_iovlen - HEADER_SIZE;
	*body = message->content->msg_iov + HEADER_SIZE;
}

// Verifi si message contient un header conforme :
// iovec 0 (magic) de taille 1
// iovec 1 (version) de taille 1
// iovec 2 (length) de taille 2
// Et aussi que length correspond bien a la quantite de donnees du body
int message_is_ready(Message * message, Error * error) {
	SET_ERROR(error, NO)
	CHECK_NULL_POINTER(error, message, 0)

	// On recupere le magic en verifiant qu'il existe bien
	message_get_magic(message, error);
	if(ERROR(*error)) {
		// S'il n'existe pas, le message n'est pas valide
		printf("Invalid magic. NO = %d. error = %d\n", NO, *error);
		return FALSE;
	}

	// De meme pour version
	message_get_version(message, error);
	if(ERROR(*error)) {
		printf("Invalid version\n");
		return FALSE;
	}

	// Aussi pour length
	uint16_t length = message_get_body_len(message, error);
	if(ERROR(*error)) {
		printf("Invalid magic\n");
		return FALSE;
	}

	// Determinons la taille total reelle en octets du body
	uint16_t realLength = message_computer_octets_size(message->content->msg_iov, message->content->msg_iovlen, error);
	realLength -= 4; // On enleve la taille du header
	if(realLength == length) {
		return TRUE;
	}
	else {
		printf("Invalid length no match: real=%d | length=%d\n", realLength, length);
		return FALSE;
	}
}

/*compte le nombre d'octets de donnees contenu dans count iovec de vecs*/
uint16_t message_computer_octets_size(struct iovec * vecs, size_t count, Error * error) {
	SET_ERROR(error, NO)
	CHECK_NULL_POINTER(error, vecs, 0);
	uint16_t len = 0;

	for (int i = 0; i < count; ++i) {
		struct iovec vec = vecs[i];
		len += vec.iov_len;
	}
	
	return len;
}

/*Retourne l'adresse contenu dans le message (envoi ou reception)*/
void message_get_addr(const Message * message, struct sockaddr_in6 * addr, unsigned int * addrlen, Error * error) {
	SET_ERROR(error, NO)
	CHECK_NULL_POINTER(error, message,)
	CHECK_NULL_POINTER(error, addr,)
	*addr = *((struct sockaddr_in6 *)message->content->msg_name);
	*addrlen = message->content->msg_namelen;
}

/*Ajoute bufferlen octets de buffer dans le message sous forme de iovec de taille vecSize octets
(Sauf potentiellement le dernier)
Ne verifi pas le header du message*/
void message_add_buffer(Message * message, char * buffer, unsigned int bufferlen, size_t vecSize, Error * error) {
	SET_ERROR(error, NO)
	CHECK_NULL_POINTER(error, message,)
	CHECK_NULL_POINTER(error, buffer,)

	int end;
	if(bufferlen >= vecSize) {
		end = bufferlen / vecSize + message->content->msg_iovlen;
	}
	else {
		end = message->content->msg_iovlen + 1;
	}
	int start = message->content->msg_iovlen;
	for (int i = start; i < end; ++i) {
		message->content->msg_iov[i].iov_base = malloc(vecSize);
		memcpy(message->content->msg_iov[i].iov_base, buffer+(i-start)*vecSize, vecSize);
		if(i == end-1) {
			message->content->msg_iov[i].iov_len = bufferlen - (i - start)*vecSize;
		}
		else {
			message->content->msg_iov[i].iov_len = vecSize;
		}
		message->content->msg_iovlen++;
	}
}

/*Ecris bufferLen octets du message dans buffer*/
void message_get_body_as_buffer(Message * message, char * buffer, unsigned int bufferLen, Error * error) {
	SET_ERROR(error, NO)
	CHECK_NULL_POINTER(error, message,)
	CHECK_NULL_POINTER(error, buffer,)

	FOR_DEBUG(struct iovec vec = message->content->msg_iov[0];
		printf("iovec %d = %s | size = %ld\n", 0, (char*)vec.iov_base, vec.iov_len);
		vec = message->content->msg_iov[1];
		printf("iovec %d = %s | size = %ld\n", 1, (char*)vec.iov_base, vec.iov_len);
		
		
		vec = message->content->msg_iov[2];
		printf("iovec %d = %d | size = %ld\n", 2, (*((unsigned short*)vec.iov_base)), vec.iov_len);
		unsigned short *l = malloc(sizeof(unsigned short));
		*l = *((unsigned short*)vec.iov_base);
		(*l) /= 256;
		printf("Length = %d\n", *l);
		free(l);
	)

	int bufferOffset = 0;
	for (int i = 3; i < message->content->msg_iovlen; ++i) {
		if(bufferLen == 0)
			return;

		struct iovec vec = message->content->msg_iov[i];
		FOR_DEBUG(
			printf("iovec %d = %s | size = %ld\n", i, (char*)vec.iov_base, vec.iov_len);
		)
		if(vec.iov_len <= bufferLen) {
			memcpy(buffer+bufferOffset, vec.iov_base, vec.iov_len);
			bufferLen -= vec.iov_len;
			bufferOffset += vec.iov_len;
		}
		else {
			memcpy(buffer+bufferOffset, vec.iov_base, bufferLen);
			return;
		}
	}
}
