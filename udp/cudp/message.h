#ifndef _MESSAGE_
#define _MESSAGE_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/uio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "error.h"

#define DEFAULT_VEC_CAPACITY 10
#define HEADER_SIZE 3
#define MAGIC_INDEX 0
#define VERSION_INDEX 1
#define LENGTH_INDEX 2

#define CORRECT_MAGIC 95
#define CORRECT_VERSION 1

#define TRUE 1
#define FALSE 0

typedef struct Message
{
	int socketfd;
	struct msghdr * content;
	unsigned int attribCapacity;
} Message;

/* Creation d'un nouveau message sans formater sont contenu.
Fait une copie de addr*/
Message * message_create(const struct sockaddr_in6 * addr, int socketfd, unsigned int attribCapacity);

/*Change l'adresse d'un message, libere la memoire de l'adresse precedente*/
void message_set_addr(Message * message, struct sockaddr_in6 *addr);

/* Creation d'un nouveau message en formatant les champ de content pour recevoir l'en-tete d'un message.
Fait une copie de addr*/
Message * message_create_for_receive(const struct sockaddr_in6 * addr, int socketfd, unsigned int attribCapacity, unsigned int attribSize);

void message_init(Message * message);

// detrui un message en profondeur
void message_destroy(Message * message);

// Fixe les valeurs du header de message
// Doit etre appelle avant d'ajouter des attributs dans le body
void message_set_header(Message * message, char magic, char version, uint16_t length, Error * error);

// Ajoute un iovec au message
// Renvoie une erreur s'il n'y a plus de place ou s'il n'y a pas de header
void message_add_attribut(Message * message, struct iovec attrib, Error * error);

// Ajoute nbrAttribs iovec au message
// Renvoie une erreur s'il n'y a plus de place ou s'il n'y a pas de header
void message_add_attributs(Message * message, struct iovec * attribs, size_t nbrAttribs, Error * error);

// Ajoute les iovec contenu dans 'content' au message
// Renvoie une erreur s'il n'y a plus de place ou s'il n'y a pas de header
void message_add_attributs_from_content(Message * message, struct msghdr * content, Error * error);

// Retourne true si le magic du header du message a pour valeur CORRECT_MAGIC
// Et false sinon
// Donne une erreur s'il n'y a pas encore de magic
int message_is_magic_correct(Message * message, Error * error);

// Retourne la valeur du magic du header de message
// Donne une erreur s'il n'y a pas encore de magic
char message_get_magic(Message * message, Error * error);

// Retourne la valeur de la version du header de message
// Donne une erreur s'il n'y a pas encore de version
char message_get_version(Message * message, Error * error);

// Retourne la valeur de la taille du body contenu dans le header de message
// Donne une erreur s'il n'y a pas encore de length du body
uint16_t message_get_body_len(Message * message, Error * error);

// Retourne dans la valeur pointee par body (sa valeur est un pointer)
// la liste des iovec composent le body du message
void message_get_body(Message * message, struct iovec ** body, size_t * countBodyVec, Error * error);

// Verifi si message contient un header conforme :
// iovec 0 (magic) de taille 1
// iovec 1 (version) de taille 1
// iovec 2 (length) de taille 2
// Et aussi que length correspond bien a la quantite de donnees du body
int message_is_ready(Message * message, Error * error);

/*compte le nombre d'octets de donnees contenu dans count iovec de vecs*/
uint16_t message_computer_octets_size(struct iovec * vecs, size_t count, Error * error);

/*Retourne l'adresse contenu dans le message (envoi ou reception)*/
void message_get_addr(const Message * message, struct sockaddr_in6 * addr, unsigned int * addrlen, Error * error);

/*Ajoute bufferlen octets de buffer dans le message sous forme de iovec de taille vecSize
(Sauf potentiellement le dernier)
Ne verifi pas le header du message*/
void message_add_buffer(Message * message, char * buffer, unsigned int bufferlen, size_t vecSize, Error * error);

/*Ecris bufferLen octets du message dans buffer*/
void message_get_body_as_buffer(Message * message, char * buffer, unsigned int bufferLen, Error * error);

#endif