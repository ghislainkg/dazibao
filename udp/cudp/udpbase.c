#include "udpbase.h"

#define TRUE 1
#define FALSE 0

/* Genere la sockaddr_in6associee aux nom d'hote et port
 avec la bonne famille d'adresse*/
void udp_get_sockaddr(const char * hostname, const char * port,
	struct sockaddr_in6* addr, Error * error) {
	SET_ERROR(error, NO)
	if(addr == NULL) {
		*error = NULL_POINTER;
		return;
	} 
	struct addrinfo *infos;

	struct addrinfo hint;
	memset(&hint, 0, sizeof(struct addrinfo));
	hint.ai_flags = AI_V4MAPPED | AI_ALL;
	int res_getaddrinfo = getaddrinfo(hostname, port, &hint, &infos);
	if(res_getaddrinfo != 0) {
		printf("Fail to getaddrinfo : %s", gai_strerror(res_getaddrinfo));
		*error = FAIL_SOCKADDR;
		return;
	}

	*addr = *((struct sockaddr_in6 *) infos->ai_addr);

	freeaddrinfo(infos);
}

/*La meme chose que udp_get_sockaddr sauf que le port est un entier*/
void udp_get_sockaddr_portnumber(const char * hostname, uint16_t port,
	struct sockaddr_in6* addr, Error * error) {
		char * portStr = malloc(10);
		sprintf(portStr, "%d", port);
		udp_get_sockaddr(hostname, portStr, addr, error);
		
		free(portStr);
}

/*Cette fonction fait le fd rejoindre un groupe multicast*/
static int multicast_join_group(int fd, int family, char * adresseMulti, int indexInterface) {
    struct in6_addr addr;
    inet_pton(family, adresseMulti, &addr);

    struct ipv6_mreq mreq;
    mreq.ipv6mr_interface = indexInterface;
    mreq.ipv6mr_multiaddr = addr;
    int res = setsockopt(fd, IPPROTO_IPV6, IPV6_JOIN_GROUP, &mreq, sizeof(mreq));
    if(res < 0) {
        perror("Fail joining group");
        return -1;
    }
	return 1;
}

/* Retourne un nouveau descripteur de fichier pour un socket de famille family
(devrai etre celle retourne par getSockAddr)
Ce descripteur de fichier est inscrit au groupe multicast d'adresse multicastAddress,
 de port multicastPort et via l'interfacemulticastInterface
 pour utiliser les valeur par defaut (definie ci dessus) :
 - multicastAddress = NULL
 - multicastPort <= 0
 - multicastInterface = NULL*/
int udp_get_socketfd(char * multicastAddress, int multicastPort, char * multicastInterface, Error * error) {
	SET_ERROR(error, NO)
	int domain = AF_INET6;
	int socketfd = socket(domain, SOCK_DGRAM, 0);
	if(socketfd < 0) {
		perror("Fail to create socket");
		*error = FAIL_SOCKETFD;
		return 0;
	}

	if(multicastAddress == NULL) {
		multicastAddress = MULTICAST_ADDR;
	}
	if(multicastPort <= 0) {
		multicastPort = MULTICAST_PORT;
	}
	if(multicastInterface == NULL) {
		multicastInterface = MULTICAST_INTERFACE_NAME;
	}

	struct sockaddr_in6 * addr = malloc(sizeof(struct sockaddr_in6));
    addr->sin6_family = AF_INET6;
    addr->sin6_port = htons(multicastPort);
    inet_pton(addr->sin6_family, multicastAddress, &(addr->sin6_addr));
    addr->sin6_scope_id = if_nametoindex(multicastInterface);
	if(!multicast_join_group(socketfd, AF_INET6, multicastAddress, addr->sin6_scope_id)){
		printf("\n\nNe peut pas se connecter au groupe multicast\n\n");
	}
	free(addr);

	return socketfd;
}

/*Bind la socket au port et interface passe en parrametre*/
void udp_bind_socket_to_port(int socketfd, char * interface, int family, const char *port, Error * error) {
	SET_ERROR(error, NO)
	if(port == NULL) {
		*error = NULL_POINTER;
		return;
	} 
	struct sockaddr_in6 server;
	memset(&server, 0, sizeof(server));
	server.sin6_family = family;
	server.sin6_port = htons(atoi(port));

	if(interface != NULL) {
		server.sin6_scope_id = if_nametoindex(interface);
	}

	int res = bind(socketfd, (struct sockaddr *)&server, sizeof(server));
	if(res < 0) {
		perror("Fail to bind address to sile descriptor");
		*error = FAIL_BIND;
	}
}

/*Envoi le buffer du Message a l'adresse du message via le socket du message*/
void udp_send(Message * message, Error * error) {
	SET_ERROR(error, NO)
	if(message == NULL) {
		*error = NULL_POINTER;
		return;
	} 
	if(!udp_wait_message(message, WRITE, 1, 0)) {
		*error = FAIL_SEND;
		return;
	}
	int res = sendmsg(message->socketfd, message->content, 0);
	if(res < 0) {
		perror("Fail to send response to client");
		*error = FAIL_SEND;
	}
}

/*L'adresse de message sera changer en l'adresse de multicast*/
void udp_multicast_send(Message * message, char * multicastAddress, int multicastPort, char * multicastInterface, Error * error) {
	
	FOR_DEBUG(
		printf("|%s|\n", multicastAddress);
		printf("|%d|\n", multicastPort);
		printf("|%s|\n", multicastInterface);
	)
	
	struct sockaddr_in6 * addr = malloc(sizeof(struct sockaddr_in6));
    addr->sin6_family = AF_INET6;
    addr->sin6_port = htons(multicastPort);
    inet_pton(addr->sin6_family, multicastAddress, &(addr->sin6_addr));
    addr->sin6_scope_id = if_nametoindex(multicastInterface);

	message->content->msg_name = addr;
	message->content->msg_namelen = sizeof(struct sockaddr_in6);
	SET_ERROR(error, NO)
	if(message == NULL) {
		*error = NULL_POINTER;
		return;
	} 
	if(!udp_wait_message(message, WRITE, 1, 0)) {
		*error = FAIL_SEND;
		return;
	}
	int res = sendmsg(message->socketfd, message->content, 0);
	if(res < 0) {
		perror("Fail to send message");
		*error = FAIL_SEND;
	}
}

/*Recoit un message via le socket du Message,
ecris le resultat dans le buffer du message et
l'adresse de l;emeteur dans l'adresse du message*/
void udp_receive(Message * message, Error * error) {
	SET_ERROR(error, NO)
	if(message == NULL) {
		*error = NULL_POINTER;
		return;
	} 
	if(!udp_wait_message(message, READ, 1, 0)) {
		*error = FAIL_RECEIVE;
		return;
	}
	int res = recvmsg(message->socketfd, message->content, 0);
	if(res < 0) {
		perror("Fail to receive data from client");
		*error = FAIL_RECEIVE;
	}
}

/*Attends pendant second secondes et millis millisecondes que l'on puisse lire ou ecrire
dans le socket du Message*/
int udp_wait_message(const Message * message, WaitType type,
	int second, int millis)
{
	fd_set fdset;
	FD_ZERO(&fdset);
	FD_SET(message->socketfd, &fdset);
	struct timeval tv = {second, millis};
	int res;
	if(type == READ)
		res = select(message->socketfd+1, &fdset, NULL, NULL, &tv);
	else
		res = select(message->socketfd+1, NULL, &fdset, NULL, &tv);
	if(res < 0) {
		perror("Fail to select");
		return FALSE;
	}
	else if(res == 0) {
		return FALSE;
	}
	else {
		return TRUE;
	}
}

/*Retourne une chaine de caracteres contenant l'ip et le port*/
char * udp_sockaddr_to_char(const struct sockaddr_in6 * serveraddr, Error * error) {
	SET_ERROR(error, NO)
	if(serveraddr == NULL){
		*error = NULL_POINTER;
		return NULL;
	}
	char * address = malloc(100);
	memset(address, 0, 100);
	inet_ntop(serveraddr->sin6_family, &(serveraddr->sin6_addr.s6_addr), address, 100);
	char * res = malloc(100);
	memset(res, 0, 100);
	sprintf(res, "%s\\%d", address, ntohs(serveraddr->sin6_port));
	free(address);
	return res;
}

/*Ecris dans *ip et *port l'adresse ip et le port de addr*/
void udp_sockaddr_to_char_split(const struct sockaddr_in6 * serveraddr, char ** ip, char ** port, Error * error) {
	SET_ERROR(error, NO)
	if(serveraddr == NULL){
		*error = NULL_POINTER;
		return;
	}
	char * address = malloc(100);
	memset(address, 0, 100);
	inet_ntop (serveraddr->sin6_family, &(serveraddr->sin6_addr.s6_addr), address, 100);
	*ip = address;
	*port = malloc(6);
	memset(*port, 0, 6);
	sprintf(*port, "%d", ntohs(serveraddr->sin6_port));
}

/*Verifi si deux adresse son les meme*/
int udp_address_equal(const struct sockaddr_in6* addr1, unsigned int size1,
	const struct sockaddr_in6* addr2, unsigned int size2, Error * error)
{
	SET_ERROR(error, NO)
	if(addr1 == NULL || addr2 == NULL) {
		*error = NULL_POINTER;
		return FALSE;
	}
	if(size1 == size2 &&
		addr1->sin6_family == addr2->sin6_family &&
		memcmp(addr1->sin6_addr.s6_addr, addr2->sin6_addr.s6_addr, 16) == 0 && 
		addr1->sin6_port == addr2->sin6_port) {
		return TRUE;
	}
	return FALSE;
}
