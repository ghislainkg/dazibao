#include "mainprocess.h"

/*Cette fonction est appeler quand la table des voisins a besoin de plus de voisin.
Elle envoie a un voisin aleatoire un Neighbour Request.
S'il n'y a pas de voisin, elle ne fait rien.*/
void main_on_need_voisin(MainProcess * process) {
	printf("\nSEND MESSAGE TO RANDOM VOISIN\n");
	Error error;

	// On choisi aleatoirement a qui on envoie
	if(process->tableVoisins->nbrVoisins > 0) {
		// On recupere une requete Neighbour Request
		requete * req = neighbour_request();
		
		// On recupere un voisin aleatoire
        Voisin * v = table_get_random_voisin(process->tableVoisins);
		if (v == NULL) {
			// S'il n'y en a pas, on ne fait rien
			return;
		}
		
		// On initialise un message notre message
		Message * toSendMessage = message_create(v->addr, process->socketFd, 200);

		// On fixe son en tete
		message_set_header(toSendMessage, CORRECT_MAGIC, CORRECT_VERSION, req->length, &error);
		if(ERROR(error)) {
			printf("N'a pas pu fixer l'en tete du message\n");
			message_destroy(toSendMessage);
			free(req);
			return;
		}

		// On le rempli avec la requete
		message_add_buffer(toSendMessage, req->body, req->length, 10, &error);
		if(ERROR(error)) {
			printf("N'a pas pu fixer le corps du message\n");
			message_destroy(toSendMessage);
			free(req);
			return;
		}

		FOR_DEBUG(// Debuggage
            printf("MESSAGE TO SEND\n");
            char * msgBuffer = malloc(500);
            memset(msgBuffer, 0, 500);
            message_get_body_as_buffer(toSendMessage, msgBuffer, 500, &error);
            printf("MESSAGE TO SEND : %s\n", msgBuffer);
    		free(msgBuffer);
		)

		// On verifi que tout est ok
		if(message_is_ready(toSendMessage, &error)) {
			printf("Message is ready\n");
		}
		else {
			printf("Message not ready\n");
			return;
		}

		// On envoi le message
		udp_send(toSendMessage, &error);
		if(error != NO) {
			printf("Fail to send default message\n");
		}

        FOR_DEBUG(// Debuggage
            char * addrStr = udp_sockaddr_to_char(v->addr, &error);
		    printf("MESSAGE SEND TO : %s\n", addrStr);
		    free(addrStr);
		)

		message_destroy(toSendMessage);
		free(req);
	}
}

/*Cette fonction permet de recevoir un message.
Elle retourne le message recu s'il y en a et NULL si non.*/
Message * main_receive_message(MainProcess * mainProcess) {
	Error error;
	struct sockaddr_in6 addr;
	memset(&addr, 0, sizeof(struct sockaddr_in6));
	Message * receivedMessage = message_create_for_receive(&addr, mainProcess->socketFd, 4, 500);

	udp_receive(receivedMessage, &error);
	if(error != NO) {
		printf("Pas de message recu\n");
		message_destroy(receivedMessage);
		return NULL;
	}
	else {
		FOR_DEBUG(// Debuggage
            struct sockaddr_in6 receivedMessageAddr;
            memset(&receivedMessageAddr, 0, sizeof(struct sockaddr_in6));
            unsigned int receivedMessageAddrLen = 0;
            message_get_addr(receivedMessage, &receivedMessageAddr, &receivedMessageAddrLen, &error);

            char * receivedStrBuffer = malloc(500);
            memset(receivedStrBuffer, 0, 500);
            message_get_body_as_buffer(receivedMessage, receivedStrBuffer, 500, &error);
			char * ad = udp_sockaddr_to_char(&receivedMessageAddr, &error);
            printf("\nNEW MESSAGE FROM %s : %s\n", ad, receivedStrBuffer);
            free(receivedStrBuffer);
			free(ad);
		)

		return receivedMessage;
	}
}

/*Cette fonction prend en parametre un message (normalement valide par la table des voisin).
Elle passe ce message a la table de donnee qui l'analyse puis retourne une reponse.
Cette reponse est envoyer ou non (Si elle est vide)*/
void main_handle_valid_message(MainProcess * mainProcess, Message * receivedMessage) {
	Error error;

	// Fabriquons la requette recue
	traitement trait;
	memset(&trait, 0, sizeof(traitement));
	requete aux;
	trait.question = &aux;
    memset(trait.question, 0, sizeof(requete));
	// On lui donne la taille du message
	trait.question->length = message_get_body_len(receivedMessage, &error);
	if(error != NO) {
		printf("Fail getting received message length\n");
		return;
	}
	printf("Traitment created\n");
	// On lui donne le corps du message
	message_get_body_as_buffer(receivedMessage, trait.question->body, TAILLE_DATAGRAMME, &error);
	if(ERROR(error)) {
		printf("N'a pas pu fixer le corps du message recu\n");
		message_destroy(receivedMessage);
		return;
	}

	// Maintenant, traitons la requete
	traitement_general(&trait, mainProcess->tableDonnees, mainProcess->tableVoisins);
	printf("Traitement response\n");

	FOR_DEBUG(printf("nbrep = %d\n", trait.nbrep);)
	// Envoyons les reponses
	for(int i=0; i<trait.nbrep; i++) {
		main_send_requete(receivedMessage, trait.reponse[i]);
	}

	printf("RESPONSE SENT\n");
}

/*Cette fonction prend en argument une requete et l'envoie a l'adresse contenue dans le message passe en parametre.*/
void main_send_requete(Message * receivedMessage, requete requete) {
	printf("SEND\n");
	Error error;

	// On reinitialise le contenu du message
	message_init(receivedMessage);
	// On fixe un header correct
	message_set_header(receivedMessage, CORRECT_MAGIC, CORRECT_VERSION, requete.length, &error);
	if(ERROR(error)) {
		printf("N'a pas pu fixer l'en tete du message\n");
		message_destroy(receivedMessage);
		return;
	}

	// On ajoute le contenu de la requete
	message_add_buffer(receivedMessage, requete.body, (unsigned short)requete.length, TAILLE_DATAGRAMME, &error);
	if(ERROR(error)) {
		printf("N'a pas pu fixer le corps du message\n");
		message_destroy(receivedMessage);
		return;
	}

	FOR_DEBUG(
		char * msgBuffer = malloc(500);
		memset(msgBuffer, 0, 500);
		message_get_body_as_buffer(receivedMessage, msgBuffer, 500, &error);
		printf("MESSAGE TO SEND : %s\n", msgBuffer);
		free(msgBuffer);
	)

	// On verifie quand meme que le message est bien pret et correct
	if(message_is_ready(receivedMessage, &error)) {
		printf("Response is ready\n");
	}
	else {
		printf("Response not ready\n");
		return;
	}

	printf("Response message created\n");

	// Et on envoi le message
	udp_send(receivedMessage, &error);
	if(ERROR(error)) {
		printf("Le message n'a pas ete envoye\n");
	}
}

/*Cette fonction envoie un Network Hash a un voisin aleatoire*/
void main_send_network_hash(MainProcess * mainProcess) {
	printf("SEND NETWORK HASH\n");
    Error error;

	// On cree la requete Network Hash
    requete* req = network_hash_send(mainProcess->tableDonnees);

	// On rempli le message
    Message * toSendMessage = message_create(NULL, mainProcess->socketFd, 200);
    message_set_header(toSendMessage, CORRECT_MAGIC, CORRECT_VERSION, req->length, &error);
	if(ERROR(error)) {
		printf("N'a pas pu fixer l'en tete du message\n");
		message_destroy(toSendMessage);
		free(req);
		return;
	}
    message_add_buffer(toSendMessage, req->body, req->length, 10, &error);
	if(ERROR(error)) {
		printf("N'a pas pu fixer le corps du message\n");
		message_destroy(toSendMessage);
		free(req);
		return;
	}

    FOR_DEBUG( // Debuggage
        char * msgBuffer = malloc(500);
        memset(msgBuffer, 0, 500);
        message_get_body_as_buffer(toSendMessage, msgBuffer, 500, &error);
        printf("MESSAGE TO SEND : %s\n", msgBuffer);
        free(msgBuffer);
	)

    if(message_is_ready(toSendMessage, &error)) {
        printf("Message is ready\n");
    }
    else {
        printf("Message not ready\n");
        return;
    }

	for (int i = 0; i < mainProcess->tableVoisins->capacity; ++i) {
		Voisin * v = mainProcess->tableVoisins->voisins[i];
		if(v != NULL) {
			message_set_addr(toSendMessage, v->addr);

			// On envoi le message
			udp_send(toSendMessage, &error);
			if(ERROR(error)) {
				printf("Le message n'a pas ete envoye\n");
			}
			else {
				char * addrStr = udp_sockaddr_to_char(v->addr, &error);
				printf("NETWORK HASH SEND TO : %s\n", addrStr);
				free(addrStr);
			}			
		}
	}

    message_destroy(toSendMessage);
    free(req);
}

/*Cette fonction envoie un network hash sur le reseau en multicast.*/
void main_send_multicast_networkhash(MainProcess * mainProcess) {
	printf("SEND MULTICAST NETWORK HASH\n");
	Error error;

    requete* req = network_hash_send(mainProcess->tableDonnees);

    Message * toSendMessage = message_create(NULL, mainProcess->socketFd, 200);

    message_set_header(toSendMessage, CORRECT_MAGIC, CORRECT_VERSION, req->length, &error);
	if(ERROR(error)) {
		printf("N'a pas pu fixer l'en tete du message\n");
		message_destroy(toSendMessage);
		free(req);
		return;
	}
    message_add_buffer(toSendMessage, req->body, req->length, 10, &error);
	if(ERROR(error)) {
		printf("N'a pas pu fixer le corps du message\n");
		message_destroy(toSendMessage);
		free(req);
		return;
	}

    FOR_DEBUG( // Debuggage
        printf("MULTICAST SEND NETWORK HASH\n");
        char * msgBuffer = malloc(500);
        memset(msgBuffer, 0, 500);
        message_get_body_as_buffer(toSendMessage, msgBuffer, 500, &error);
        printf("MESSAGE TO SEND : %s\n", msgBuffer);
        free(msgBuffer);
	)

    if(message_is_ready(toSendMessage, &error)) {
        printf("Message is ready\n");
    }
    else {
        printf("Message not ready\n");
		message_destroy(toSendMessage);
		free(req);
		return;
    }

    // On envoi le message multicast
	udp_multicast_send(
		toSendMessage,
		mainProcess->multicastAddress,
		mainProcess->multicastPort,
		mainProcess->multicastInterface,
		&error);
    if(ERROR(error)) {
        printf("Fail to send default message\n");
		return;
    }
    message_destroy(toSendMessage);
    free(req);
}

/*Cette fonction verifie si on a recu le message d'arret du programme*/
int main_receive_stop() {
	fd_set fdset;
	FD_ZERO(&fdset);
	FD_SET(STDOUT_FILENO, &fdset);
	struct timeval tv = {0, 100};
	int res = select(STDOUT_FILENO+1, &fdset, NULL, NULL, &tv);
	if(res < 0) {
		FOR_DEBUG(perror("Fail to select");)
		return FALSE;
	}
	else if(res == 0) {
		return FALSE;
	}
	else {
		char response;
		res = read(STDOUT_FILENO, &response, 1);
		if(res < 0) {
			return FALSE;
		}
		if(response == 'o') {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
}
