#include "mainprocess.h"

/*Cree un processus de communication.
- Initialise la table des voisins a partir du fichier de nom <tableFileName>.
- Initialise la table de donnees.
- cree une socket de communication inscrit a l'adresse multicast <multicastAddress>.
cette socket communique via le port <myport> et par l'interface de nom <interface> (si il n'est pas <= 0)*/
MainProcess * main_process_create(char * tableFileName, const char * myport, char * data,
	char * multicastAddress, int multicastPort, char * interface) {
	MainProcess * mainProcess = malloc(sizeof(MainProcess));
	memset(mainProcess, 0, sizeof(MainProcess));

	long currentTime = time(NULL);

	// On recupere le descripteur du fichier des adresse ip
	int fd = open(tableFileName, O_RDWR);
	if(fd < 0) {
		perror("Ne peut pas lire le fichier de voisins");
		return NULL;
	}
	mainProcess->tableFd = fd;

	Error error;
	// initialisons la table de voisins
	printf("\nINIT NEIGHBOURS TABLE\n");
	mainProcess->tableVoisins = table_get_table_from_file(tableFileName, myport);
	table_print(mainProcess->tableVoisins, currentTime);

	// initialisons la table de donnees
	printf("INIT DATA TABLE\n");
	mainProcess->tableDonnees = init(data);
	if(mainProcess->tableDonnees == NULL) {
		table_destroy(mainProcess->tableVoisins);
		exit(1);
	}

	// On fixe les parametres multicast
	if(multicastAddress == NULL) {
		mainProcess->multicastAddress = MULTICAST_ADDR;
	}
	if(multicastPort <= 0) {
		mainProcess->multicastPort = MULTICAST_PORT;
	}
	if(interface == NULL) {
		mainProcess->multicastInterface = MULTICAST_INTERFACE_NAME;
	}

	// On se bind au port
	printf("BIND SOCKET TO PORT %s\n", myport);
	mainProcess->socketFd = udp_get_socketfd(
		mainProcess->multicastAddress,
		mainProcess->multicastPort,
		mainProcess->multicastInterface,
		&error);
	if(error != NO) {
		table_destroy(mainProcess->tableVoisins);
		exit(1);
	}
	udp_bind_socket_to_port(mainProcess->socketFd, interface, AF_INET6, myport, &error);
	if(error != NO) {
		table_destroy(mainProcess->tableVoisins);
		exit(1);
	}

	return mainProcess;
}

/*Libere toute la memoire du processus de communication*/
void main_destroy(MainProcess * mainProcess) {
	// Detruisons la table des voisins
	table_destroy(mainProcess->tableVoisins);
	//Sauvegardons puis  Detruisons la table de donnees
	garder_trace(mainProcess->tableDonnees);
	if(mainProcess->tableDonnees != NULL) {
		free(mainProcess->tableDonnees);
	}

	// Fermons le descripteur du fichier des adresses
	close(mainProcess->tableFd);
	// Fermons notre socket
	close(mainProcess->socketFd);

	free(mainProcess);
}

/*La boucle executant un processus de communication*/
void main_process(MainProcess * mainProcess) {
	printf("Entrer au clavier 'o' suivi de la touche [Entrer] pour arreter\n");
	long currentTime = time(NULL);
	// Le moment du dernier envoie de NetworkHash
	long lastNetworkHashMessageTime = currentTime;
	// Le moment du dernier envoie de NetworkHash en multicast
	long lastMultiCast = 0;

	long last_stop_test = currentTime;

	while(1) {
		if(currentTime - last_stop_test >=5 && main_receive_stop()) {
			break;
		}

		printf("\nMAIN PROCESS LOOP #######################################################################################\n");
		currentTime = time(NULL);
		

		printf("\nNEIGHBOURS TABLE\n");
		table_print(mainProcess->tableVoisins, currentTime);

		//printf("DATA TABLE\n");
		//print_table(mainProcess->tableDonnees);

		// supprimer les vieux voisins
		table_refresh(mainProcess->tableVoisins, currentTime);

		if(table_check_empty_time(mainProcess->tableVoisins, currentTime)) {
			// La table a besoin de nouveau voisins, on envoie un message
			main_on_need_voisin(mainProcess);
		}

		if(currentTime - lastNetworkHashMessageTime >= 20) {
			// 20 secondes, on envoie un networkhash
			main_send_network_hash(mainProcess);
			lastNetworkHashMessageTime = currentTime;
		}

		if(currentTime - lastMultiCast >= 100) {
			// 100 secondes, on envoie des networkhash
			main_send_multicast_networkhash(mainProcess);
			lastMultiCast = currentTime;
		}

		// On recoit un message
		Message * receivedMessage = main_receive_message(mainProcess);
		if(receivedMessage == NULL)
			continue;

		printf("\nMESSAGE PROCCESSING\n");
		if(table_validate_message(mainProcess->tableVoisins, receivedMessage, currentTime)) {
			// Si le message est valide
			printf("VALID MESSAGE\n");
			main_handle_valid_message(mainProcess, receivedMessage);
		}
		else {
			printf("Invalid\n");
		}
		message_destroy(receivedMessage);
	}

	printf("Fin du programme\n");
}
