#ifndef _MAIN_PROCESS_
#define _MAIN_PROCESS_

#define _GNU_SOURCE

#include <stdio.h>
#include <time.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "../data/table.h"
#include "../data/voisin.h"

#include "../../donnees/gestion_table.h"

/*"Processus de communication" a des references vers toutes le donnees necessaire
au fonctionnement du programe*/
typedef struct MainProcess
{
	TableVoisins * tableVoisins;
	table * tableDonnees;
	int tableFd;
	int socketFd;

	char * multicastAddress;
	int multicastPort;
	char * multicastInterface;
} MainProcess;

/*Cree un processus de communication.
- Initialise la table des voisins a partir du fichier de nom <tableFileName>.
- Initialise la table de donnees.
- cree une socket de communication inscrit a l'adresse multicast <multicastAddress>.
cette socket communique via le port <myport> et par l'interface de nom <interface> (si il n'est pas <= 0)*/
MainProcess * main_process_create(char * tableFileName, const char * myport, char * data,
	char * multicastAddress, int multicastPort, char * multicastInterface);

/*Libere toute la memoire du processus de communication*/
void main_destroy(MainProcess * mainProcess);

/*La boucle executant un processus de communication*/
void main_process(MainProcess * mainProcess);



/*Cette fonction est appeler quand la table des voisins a besoin de plus de voisin.
Elle envoie a un voisin aleatoire un Neighbour Request.
S'il n'y a pas de voisin, elle ne fait rien.*/
void main_on_need_voisin(MainProcess * process);

/*Cette fonction permet de recevoir un message.
Elle retourne le message recu s'il y en a et NULL si non.*/
Message * main_receive_message(MainProcess * mainProcess);

/*Cette fonction prend en parametre un message (normalement valide par la table des voisin).
Elle passe ce message a la table de donnee qui l'analyse puis retourne une reponse.
Cette reponse est envoyer ou non (Si elle est vide)*/
void main_handle_valid_message(MainProcess * mainProcess, Message * receivedMessage);

/*Cette fonction prend en argument une requete et l'envoie a l'adresse contenue dans le message passe en parametre.*/
void main_send_requete(Message * receivedMessage, requete requete);

/*Cette fonction envoie un Network Hash a un voisin aleatoire*/
void main_send_network_hash(MainProcess * mainProcess);

/*Cette fonction envoie un network hash sur le reseau en multicast.*/
void main_send_multicast_networkhash(MainProcess * mainProcess);

/*Cette fonction verifie si on a recu le message d'arret du programme*/
int main_receive_stop();

#endif
