#ifndef _TABLE_
#define _TABLE_

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "../cudp/udpbase.h"
#include "voisin.h"

typedef struct TableVoisins
{
	Voisin ** voisins;

	/*Le nombre actuel de voisins*/
	unsigned int nbrVoisins;

	/*La capacite de la table*/
	unsigned int capacity;

	// Un voisin doit communiquer au moins toutes les timeout secondes
	long timeout;

	/* Le moment ou il y a moins de nbrMinVoisins voisins dans la table
	0 si actuellement il y en a plus*/
	long emptyStartTime;

	/*Le temps maximale en seconde pendant lequel la table peut contenir moins de nbrMinVoisins voisins*/
	long emptynessTimeLimit;

	/*Le nombre min de voisins*/
	int nbrMinVoisins;

	/*Le nombre max de voisins*/
	int nbrMaxVoisins;

} TableVoisins;

typedef struct IpPort
{
	const char * ip;
	uint16_t port;
} IpPort;

/*Cree une nouvelle table de voisin*/
TableVoisins * table_create(
	unsigned int capacity,
	long timeout,
	int nbrMinVoisins,
	int nbrMaxVoisins,
	long emptynessTimeLimit);

/*Detruis la table et tous ses voisins*/
void table_destroy(TableVoisins * table);

/*Supprime le voisin de la table s'il n'est pas permanent*/
int table_add_voisin(TableVoisins * table, Voisin * voisin, long currentTime);

/*Supprime le voisin de la table s'il n'est pas permanent et libere la memoire de ce voisin*/
int table_remove_voisin_free(TableVoisins * table, Voisin * voisin);

/*Teste si un voisin est dans une table de voisins*/
int table_is_voisin_in_table(TableVoisins * table, Voisin * voisin);

/*Verifi si l'adresse du Message est dans la table
Si oui retourne true
Si non verifi s'il y a encore de la place
	Si oui la nouvelle adresse est ajoute
	Si non retourne false*/
int table_validate_message(TableVoisins * table, Message * message, long currentTime);



/*Elimine les voisins qui n'on pas communiquer dans le temps imparti*/
void table_refresh(TableVoisins * table, long currentTime);

/*Retourne true si la table a besoin de plus de voisins*/
int table_check_empty_time(TableVoisins * table, long currentTime);

/*Retourne un voisin aleatoire dans la table*/
Voisin * table_get_random_voisin(const TableVoisins * table);

/*Affiche la table de voisins*/
void table_print(TableVoisins * table, long currentTime);

/*Retourne un couple adresse ip et port (IpPort) */
IpPort table_get_random_ip_port(TableVoisins * table);

/*Cree une table de voisin, li les adresse et port contenu dans le fichier de nom filename,
Ajoute ces adresse comme voisin permanent*/
TableVoisins * table_get_table_from_file(const char * filename, const char * myport);

#endif