#ifndef _VOISIN_
#define _VOISIN_

#include <stdio.h>
#include <string.h>

#include "../cudp/udpbase.h"

typedef struct Voisin
{
	struct sockaddr_in6 * addr;
	unsigned int addrlen;
	long lastcom;
	int isPermanent;
} Voisin;

Voisin * voisin_create(struct sockaddr_in6 * addr, unsigned int addrlen, int isPermanent);
void voisin_destroy(Voisin * voisin);
int voisin_own_message(const Voisin * voisin, const Message * message);
int voisin_is_timeup(const Voisin * voisin, long timeLimit, long current);

#endif