#include "table.h"

#define TRUE 1
#define FALSE 0

/*Cree une nouvelle table de voisin*/
TableVoisins * table_create(
	unsigned int capacity,
	long timeout,
	int nbrMinVoisins,
	int nbrMaxVoisins,
	long emptynessTimeLimit)
{
	TableVoisins * table = malloc(sizeof(TableVoisins));
	memset(table, 0, sizeof(TableVoisins));

	table->nbrVoisins = 0;
	table->capacity = capacity;
	table->nbrMinVoisins = nbrMinVoisins;
	table->nbrMaxVoisins = nbrMaxVoisins;

	table->voisins = malloc(sizeof(Voisin*)*capacity);
	for (int i = 0; i < table->capacity; ++i) {
		table->voisins[i] = NULL;
	}

	table->timeout = timeout;
	table->emptyStartTime = 0;
	table->emptynessTimeLimit = emptynessTimeLimit;

	return table;
}

/*Detruis la table et tous ses voisins*/
void table_destroy(TableVoisins * table) {
	for (int i = 0; i < table->capacity; ++i) {
		if(table->voisins[i] != NULL) {
			voisin_destroy(table->voisins[i]);
		}
	}
	free(table->voisins);
	free(table);
}

/*Supprime le voisin de la table s'il n'est pas permanent*/
int table_add_voisin(TableVoisins * table, Voisin * voisin, long currentTime) {
	if(table_is_voisin_in_table(table, voisin)) {
		printf("Voisin deja dans la table\n");
		return 0;
	}
	for (int i = 0; i < table->capacity; ++i) {
		if(table->voisins[i] == NULL) {
			table->voisins[i] = voisin;
			table->voisins[i]->lastcom = currentTime;
			table->nbrVoisins ++;
			return 1;
		}
	}
	return 0;
}

/*Supprime le voisin de la table s'il n'est pas permanent et libere la memoire de ce voisin*/
int table_remove_voisin_free(TableVoisins * table, Voisin * voisin) {
	if(voisin->isPermanent)
		return 0;
	for (int i = 0; i < table->capacity; ++i) {
		if(table->voisins[i] == voisin) {
			table->voisins[i] = NULL;
			free(voisin);
			table->nbrVoisins --;
			return 1;
		}
	}
	return 0;
}

/*Teste si un voisin est dans une table de voisins*/
int table_is_voisin_in_table(TableVoisins * table, Voisin * voisin) {
	Error error;
	for (int i = 0; i < table->capacity; ++i) {
		if(table->voisins[i] != NULL) {
			if(udp_address_equal(
				table->voisins[i]->addr,
				sizeof(table->voisins[i]->addr),
				voisin->addr,
				sizeof(voisin->addr), &error) ) {
				return TRUE;
			}
		}
	}
	return FALSE;
}

/*Verifi si l'adresse du Message est dans la table
Si oui retourne true
Si non verifi s'il y a encore de la place
	Si oui la nouvelle adresse est ajoute
	Si non retourne false*/
int table_validate_message(TableVoisins * table, Message * message, long currentTime) {
	printf("VALIDATE MESSAGE\n");
	Error error;
	message_is_magic_correct(message, &error);
	if(ERROR(error)) {
		printf("UNCORRECT MAGIC\n");
		return FALSE;
	}

	for (int i = 0; i < table->capacity; ++i) {
		if(table->voisins[i] != NULL) {
			if(voisin_own_message(table->voisins[i], message)) {
				table->voisins[i]->lastcom = currentTime;
				printf("MESSAGE OWNER IS IN TABLE\n");
				return TRUE;
			}
		}
	}

	// L'emeteur n'est pas dans la table
	if(table->nbrVoisins < table->nbrMaxVoisins) {
		printf("ENOUGH SPACE\n");
		// Il ya encore de la place dans la table, on ajoute le nouveau voisin
		struct sockaddr_in6 * addr = malloc(sizeof(struct sockaddr_in6));
		memset(addr, 0, sizeof(struct sockaddr_in6));
		unsigned int addrlen;
		Error udperror;
		message_get_addr(message, addr, &addrlen, &udperror);
		if(ERROR(udperror))
			return FALSE;
		Voisin * voisin = voisin_create(addr, addrlen, FALSE);
		table_add_voisin(table, voisin, currentTime);
		printf("ADD MESSAGE OWNER TO TABLE\n");
		voisin->lastcom = currentTime;
		return TRUE;
	}
	else {
		printf("NOT ENOUGH SPACE : INVALID MESSAGE\n");
		return FALSE;
	}
}

/*Elimine les voisins qui n'on pas communiquer dans le temps imparti*/
void table_refresh(TableVoisins * table, long currentTime) {
	printf("\nREFRESH\n");
	for (int i = 0; i < table->capacity; ++i) {
		if(table->voisins[i] != NULL) {
			
			char * ad = udp_sockaddr_to_char(table->voisins[i]->addr, NULL);
			printf("Voisin %d : %s | lastcom %ld\n",
				i,
				ad,
				currentTime - table->voisins[i]->lastcom);
				free(ad);

			if(voisin_is_timeup(table->voisins[i], table->timeout, currentTime)) {
				if(table_remove_voisin_free(table, table->voisins[i]))
					printf("Removed\n");
			}
			else {
				printf("Ok\n");
			}
		}
	}
	printf("REFRESH END\n");
}

/*Retourne true si la table a besoin de plus de voisins*/
int table_check_empty_time(TableVoisins * table, long currentTime) {
	printf("\nCHECK EMPTY\n");
	if(table->nbrVoisins < table->nbrMinVoisins) {
		if(table->emptyStartTime == 0) {
			printf("MAJ\n");
			table->emptyStartTime = currentTime;
			return FALSE;
		}
		else if(currentTime - table->emptyStartTime >= table->emptynessTimeLimit) {
			printf("NEED\n");
			return TRUE;
		}
		else {
			printf("OK\n");
			return FALSE;
		}
	}
	else {
		printf("RESET\n");
		table->emptyStartTime = 0;
		return FALSE;
	}
	printf("CHECK EMPTY END\n");
}

/*Retourne un voisin aleatoire dans la table*/
Voisin * table_get_random_voisin(const TableVoisins * table) {
	int k = 0;
	int p = rand() % table->capacity;
	while(k < table->capacity) {
		if(table->voisins[p] != NULL) {
			return table->voisins[p];
		}
		else {
			k++;
			p++;
			if(p >= table->capacity)
				p = 0;
		}
	}
	printf("Could not find neigbour\n");
	return NULL;
}

/*Affiche la table de voisins*/
void table_print(TableVoisins * table, long currentTime) {
	printf("\nTable : size (%d)\n", table->nbrVoisins);
	for (int i = 0; i < table->capacity; ++i) {
		if(table->voisins[i] == NULL) {
			printf("%d Empty\n", i);
		}
		else {
			char * addrport = udp_sockaddr_to_char(table->voisins[i]->addr, NULL);
			printf("Voisin %d : %s, derniere communication %ld, seconde depuis = %ld\n", i, addrport, table->voisins[i]->lastcom,
				currentTime - table->voisins[i]->lastcom);
			free(addrport);
		}
	}
	printf("\n");
}


/*Retourne un couple adresse ip et port (IpPort) */
IpPort table_get_random_ip_port(TableVoisins * table) {
	Voisin * voisin = table_get_random_voisin(table);
	
	IpPort ipPort;
	memset(&ipPort, 0, sizeof(IpPort));
	ipPort.ip = (char*) &(voisin->addr->sin6_addr.__in6_u);
	ipPort.port = voisin->addr->sin6_port;

	return ipPort;
}

/*Retourne la taille d'un fichier.
Retourne -1 en cas d'erreur*/
static unsigned int system_get_file_size(const char * filename) {
    struct stat statbuf;
    int res = stat(filename, &statbuf);
    if(res < 0) {
        perror("Ne peut pas lire la taille du fichier");
        return -1;
    }

    return statbuf.st_size;
}

/*Cree une table de voisin, li les adresse et port contenu dans le fichier de nom filename,
Ajoute ces adresse comme voisin permanent*/
TableVoisins * table_get_table_from_file(const char * filename, const char * myport) {
	int fd = open(filename, O_RDONLY);

	// On recupere la taille du fichier
	unsigned int bufferSize = system_get_file_size(filename);
	if(bufferSize < 0) {
		return NULL;
	}

	// On alloue la memoire necessaire
	char * buffer = malloc(bufferSize);
	memset(buffer, 0, bufferSize);

	// On lis le fichier en entier
	int bufferlen = read(fd, buffer, bufferSize);
	if(bufferlen < 0) {
		perror("Fail to read table_data");
		free(buffer);
		return NULL;
	}

	// On cree la table
	TableVoisins * table = table_create(
	15, // Capacity
	70,// timeout
	5,// nbrMinVoisins
	15,// nbrMaxVoisins
	20// emptynessTimeLimit
	);

	// Les delimiters
	char * needelAddr = "|"; // Marque la fin d'une adresse
	char * needelPort = "\n"; // Marque la fin d'un port
	char * current = buffer;
	Error udperror;
	printf("Lecture du fichier %s\n", filename);
	while (bufferlen > 0) {

		// On cherche la fin de l'adresse ip
		char * addrEnd = memmem(current, bufferSize, needelAddr, strlen(needelAddr));
		if(addrEnd == NULL) {
			return table;
		}
		else {
			// Si on la trouve
			int addrSize = addrEnd - current;
			char * addrstr = malloc(addrSize+1);
			memcpy(addrstr, current, addrSize);
			addrstr[addrSize] = '\0';

			current += addrSize+1;
			bufferlen -= addrSize+1;

			// On cherche la fin du numero de port
			char * portEnd = memmem(current, bufferlen, needelPort, strlen(needelPort));
			if(portEnd == NULL) {
				return table;
			}
			else {
				// Si on la trouve
				int portSize = portEnd - current;
				char * portstr = malloc(portSize+1);
				memcpy(portstr, current, portSize);
				portstr[portSize] = '\0';

				printf("contiens l'adresse : %s\\%s\n", addrstr, portstr);

				current += portSize+1;
				bufferlen -= portSize+1;

				// On demande la sockaddr
				struct sockaddr_in6 * addr = malloc(sizeof(struct sockaddr_in6));
				udp_get_sockaddr(addrstr, portstr, addr, &udperror);
				if(udperror != NO) {
					printf("\tCette adresse n'est pas valide\n");
					continue;
				}
				printf("\tCette adresse est valide\n");

				// On ajoute un nouveau voisin a la table
				long currentTime = time(NULL);
				Voisin * v = voisin_create(addr, sizeof(struct sockaddr_in6), TRUE);
				table_add_voisin(table, v, currentTime);
				printf("\telle est ajoutee a la table\n");

				free(portstr);
			}
			free(addrstr);
		}
	}

	free(buffer);
	close(fd);
	return table;
}
