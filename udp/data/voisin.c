#include "voisin.h"

#define TRUE 1
#define FALSE 0

Voisin * voisin_create(struct sockaddr_in6 * addr, unsigned int addrlen, int isPermanent) {
	Voisin * voisin = malloc(sizeof(Voisin));
	memset(voisin, 0, sizeof(Voisin));
	voisin->addr = addr;
	voisin->addrlen = addrlen;
	voisin->lastcom = 0;
	voisin->isPermanent = isPermanent;

	return voisin;
}

void voisin_destroy(Voisin * voisin) {
	free(voisin->addr);
	free(voisin);
}

int voisin_own_message(const Voisin * voisin, const Message * message) {
	struct sockaddr_in6 addr;
	memset(&addr, 0, sizeof(struct sockaddr_in6));
	unsigned int addrlen = 0;
	Error udperror;
	message_get_addr(message, &addr, &addrlen, &udperror);
	if(udp_address_equal(
		voisin->addr, voisin->addrlen,
		&addr, addrlen, &udperror
	)) {
		return TRUE;
	}
	return FALSE;
}

int voisin_is_timeup(const Voisin * voisin, long timeLimit, long current) {
	return (current - voisin->lastcom) >= timeLimit;
}

