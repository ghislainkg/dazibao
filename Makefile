

obj=obj/
opt= -Wall -g

target= $(obj)dazibao.o $(obj)message.o $(obj)mainprocess.o $(obj)steps.o $(obj)table.o $(obj)udpbase.o $(obj)voisin.o $(obj)requete.o $(obj)gestion_table.o $(obj)sha1.o $(obj)sha224-256.o
dazibao: $(target)
	gcc $(opt) $(target) -o dazibao

$(obj)dazibao.o: dazibao.c
	gcc $(opt) -c dazibao.c -o $(obj)dazibao.o

#partie donnees
don=donnees/

$(obj)requete.o: $(don)requete.c $(don)requete.h
	gcc $(opt) -c $(don)requete.c -o $(obj)requete.o

$(obj)gestion_table.o: $(don)gestion_table.c
	gcc $(opt) -c $(don)gestion_table.c -o $(obj)gestion_table.o

# sha
sha=$(don)rfc6234-master/

$(obj)sha1.o: $(sha)sha1.c $(sha)sha.h $(sha)sha-private.h
	gcc $(opt) -c $(sha)sha1.c -o $(obj)sha1.o

$(obj)sha224-256.o: $(sha)sha224-256.c $(sha)sha.h $(sha)sha-private.h
	gcc $(opt) -c $(sha)sha224-256.c -o $(obj)sha224-256.o

# partie udp
udp=udp/

$(obj)mainprocess.o: $(udp)communication/mainprocess.c $(udp)communication/mainprocess.h
	gcc $(opt) -c $(udp)communication/mainprocess.c -o $(obj)mainprocess.o

$(obj)steps.o: $(udp)communication/steps.c $(udp)communication/mainprocess.h
	gcc $(opt) -c $(udp)communication/steps.c -o $(obj)steps.o

$(obj)table.o: $(udp)data/table.c $(udp)data/table.h
	gcc $(opt) -c $(udp)data/table.c -o $(obj)table.o

$(obj)udpbase.o: $(udp)cudp/udpbase.c $(udp)cudp/udpbase.h $(udp)cudp/error.h
	gcc $(opt) -c $(udp)cudp/udpbase.c -o $(obj)udpbase.o

$(obj)message.o: $(udp)cudp/message.c $(udp)cudp/message.h $(udp)cudp/error.h
	gcc $(opt) -c $(udp)cudp/message.c -o $(obj)message.o

$(obj)voisin.o: $(udp)data/voisin.c $(udp)data/voisin.h
	gcc $(opt) -c $(udp)data/voisin.c -o $(obj)voisin.o

clean: 
	rm -rf $(obj)*.o dazibao files/donnees.txt files/warning.txt
