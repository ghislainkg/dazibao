Utiliser dazibao.

Arreter le programme :
    Une fois lance le programme lancer, vous pouvez 
    entrer au clavier 'o' suivi de la touche [Entrer] pour arreter
    et ainsi l'arreter de facon propre.

Les options de demarage :

    ./dazibao -h // Pour afficher l'aide

    Voyons plus en detail chaque option :

    -a <fichier des adresses> 
        Vous permet de specifier le fichier contenant les adresses a mettre dans la table des voisin. se fichier doit etre formater comme dans cet exemple :
        ::ffff:192.168.43.50|8082
        jch.irif.fr|1212
        jch.irif.fr|1212
        jch.irif.fr|1212
        Si cette option n'est pas utilisee, le programme vas essayer de lire un fichier ./files/table.
        Si ce fichier ou celui specifie avec l;option ne sont pas accessible, le programme ne poursuivra pas.

    -d <donnee> // pour specifier la donnee a partager
        Une donnee est publiee a chaque instant par le programme. cette option vous permet de la specifier.
        Si elle n'est pas specifier, une donnee deja definie est alors utilisee par defaut

    -m <adresse multicast ipv6>
        Cette option permet de specifier l'adresse de multicast que vas utiliser le programme pour les communications en multicast.
        Si cette option n'est pas utilisee, alors c'est l'adresse par defaut qui sera utilisee : "ff12::25ea:32da:1de2:ab45"

    -mp <port de multicast>
        Cette option permet de specifier le numero de port multicast, celui sur lequel les hotes doivent etre connectes pour recevoir nos message multicast.
        C'est le port 8080 qui est utilise par defaut.

    -mi <nom de l'interface de communication>
        Cette option permet de specifier le nom de l'interface reseau a utiliser pour les communication (multicast comme unicast).

    